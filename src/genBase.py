""" Copyright OttoES 2018
"""
VERSION    = "0.1.dev5"

from parseBinXbuff import (parser, enumList  , structList,structDict, msgList,msgDict, annotateDict)

from pprint import pprint

import re

def cleanstr(s):
    if s.startswith("'''"):  s = s[3:-3]
    if s.startswith('/*'):  s = s[2:-2]
    if s.startswith('"'):  s = s[1:-1]
    #if s.endswith('*/'):  s = s[2:]
    return s

def cleanstrNL(s):
    return s.replace("\n", " ")


# globals for indents
# simply change these if you want to change the indent size
def indnt(spcs):
    if spcs <= 0: return ""
    return indnt(spcs-1) + "    "

IND1          = indnt(1)
IND2          = indnt(2)
IND3          = indnt(3)
NL            = '\n'

def addIndent(s):
    """Adds another level of indentation to the code
    """
    if s.find("\n") < 2:  # start with new line
        return s.replace("\n", "\n"+IND1)
    s = IND1 + s.replace("\n", "\n"+IND1)
    return s

def convertCamelToSnake(name):
    s1 = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', name)
    return re.sub('([a-z0-9])([A-Z])', r'\1_\2', s1).upper()

def convertSnakeToCamelCase(column):
   first, *rest = column.split('_')
   return first + ''.join(word.capitalize() for word in rest)

def parseBxbDefFile(fileName):
    parser.parseFile(fileName)
    # docgen = MarkdownGenerator()
    # s = docgen.genAll()
    # print(s)


def parseBxbDefStr(defStr):
    parser.parseString(defStr)
    # docgen = MarkdownGenerator()
    # s = docgen.genAll()
    # print(s)

def ppprint():
    pprint(enumList)
    pprint(annotateDict)
    pprint(structList)


class BaseCodeGenerator:
    codeHeader          = " Auto generated code"
    COMMENT_START       = "/* "
    COMMENT_END         = " */"
    COMMENT_LINE        = "// "
    STATEMENT_END       = ";"
    doxPre              = "\n @"         # doxygen prefix
    doxPost             = " "            # doxygen postfix
    TYPE_POSTFIX        = "_t"
    #funcNamePrefix      = "BIN_"
    funcPackCallPrefix  = "BIN_call"
    funcPackBaseName    = "pack"
    funcUnpackBaseName  = "unpack"
    funcPackInBuff      = "packIntoBuffer"
    funcUnpackInStruct  = "unpackIntoStruct"
    #funcCopyStruct      = "copyStruct"
    FUN_UNPACK_ALL_NAME = "unpackFromBuff"
    FUN_DEFAULT_AFTER_UNPACK_CALL_POSTFIX = "_callUser"   # function called after unpack
    funcCreateName      = "objFactory3"  #TODO not used
    funcCreateFromBufName= "objFactory2"
    funcCreateAndUnpackData="readStream"
    funcProcessBuffName = "CallStoreSendBuffer"
    funcUnpackNamePrefix= "BIN_unpack"
    funcProcessFunPrefix= "PROCESS_MSG_"              # prefix for the functions that process incoming messages
    singletonFuncDecl   = "\n// singleton function\n"# will be static if in a class
    privateFuncDecl     = "static "                  # will be private if in a class
    errorHandlerName    = "printf"                   # function that will be called on an error
    CONST_ERR_OUT_OF_BOUNDS = "ERR_BUFF_OUT_OF_BOUNDS"
    CONST_ERR_UNKNOWN_TAG = "ERR_TAG_UNKNOWN"
    CONST_ERR_BUFF_SIZE = "ERR_BUFF_OUT_OF_DATA"
    CONST_ERR_CRC       = "ERR_CRC_FAIL"
    CONST_ERR_NOT_EQ    = "ERR_VALUE_NOT_EQUAL"
    typeTable1          = {"bool":"bool","char":"char","int":"int"}
    typeTable2          = {"string":"char","zstring":"char","ustring":"wchar","uzstring":"wchar"}
    # these types are internally generated and not passed in by the user
    typeTableLoc        = {"CRC8":"uint8","CRC16":"uint16","CRC32":"uint32","MSG_ID16":"uint16"}
    typeTableEnum       = {"enum8":"int8","enum16":"int16","enum32":"int32"}
    typeSizeTable       = {"char":1,"bool":1,"wchar":2,"byte":1}
    NO_TYPE_NAME        = "void"
    INT_TYPE_NAME       = "int"
    BUF_TYPE_NAME       = "uint8_t"
    LEN_TYPE_NAME       = "size_t"                  # buffere length etc
    BUF_NAME            = "buff"
    #BUF_DECLARE         = BUF_TYPE_NAME +" "+BUF_NAME+"[515]"
    def     BUF_DECLARE(self,len=512):  return  self.BUF_TYPE_NAME +" "+self.BUF_NAME+"["+str(len)+"]"

    #BUF_FUN_ARG         = BUF_TYPE_NAME +" "+BUF_NAME+"[]"
    BUF_FUN_ARG         = BUF_TYPE_NAME +" "+BUF_NAME+"[]"
    buffArrType         = "uint8_t"

    BUF_POS_NAME        = "pos"
    BUF_POS_DECLARE     = LEN_TYPE_NAME +" "+BUF_POS_NAME +" = 0"
    BUF_LEN_NAME        = "bufLen"
    BUF_LEN_DECLARE     = LEN_TYPE_NAME +" "+BUF_LEN_NAME
    BUF_LEN_ARG         = LEN_TYPE_NAME +" "+BUF_LEN_NAME
    BUF_SIZE_NAME       = "bufSize"
    BUF_SIZE_DECLARE    = LEN_TYPE_NAME +" "+BUF_SIZE_NAME
    BUF_SIZE_ARG        = LEN_TYPE_NAME +" "+BUF_SIZE_NAME
    packBuffType        = "char"
    # define the languages (use the file extension)
    LANG_C              = "c"
    LANG_CPP            = "cpp"
    LANG_PY             = "py"
    LANG_JAVA           = "java"
    def __init__(self,bxbDef= None):
        if bxbDef is not None:
            pp = parser.parseString(bbxDef)
        # set the language
        self.LANG = "?"
    def pprint(self):
        pprint(enumList)
        pprint(structList)
    def findEnumMember(self,symbName):
        """Search all enums for the enum value with the name symbName
        """
        for enumm in enumList:
            for memb in enumm["values"]: 
                if memb["name"] == symbName: 
                    return enumm,memb
        return None,None
    #TODO: can it be removed?
    def lookupType(self,vartype):
        if vartype in self.typeTable2: return  self.typeTable2[vartype]
        if vartype in self.typeTable1: return  self.typeTable1[vartype]
        return vartype+self.TYPE_POSTFIX
    def lookupFieldType(self,fieldd):
        vartype = fieldd["type"]
        if vartype.startswith("enum"):
            return "enum "+fieldd["enumName"]
        if vartype in self.typeTable2: return  self.typeTable2[vartype]
        if vartype in self.typeTable1: return  self.typeTable1[vartype]
        return vartype+self.TYPE_POSTFIX
    def lookupTypeSize(self,vartype):
        # handel all the non standard cases that is in the table
        if vartype in self.typeSizeTable: return  self.typeSizeTable[vartype]
        # do the standard cases where the bit isze is embedded in the type name
        if vartype.find("8") >0 : return 1
        if vartype.find("16")>0 : return 2
        if vartype.find("32")>0 : return 4
        if vartype.find("64")>0 : return 8
        return -100000  # this is an n error
    # TODO: no need to pass struct??? 
    def getParent(self,structt):
        if not "parentName" in structt: return None
        parnt = structt["parentName"]
        return structDict[parnt]
    # def getParent2(self):
    #     if not "parentName" in self: return None
    #     parnt = self["parentName"]
    #     return structDict[parnt]
    # statements that can be overridden for different languages
    def makeStatementIf(self,condition):
        return "if ("+condition+") {" + NL
    def makeStatementElse(self):
        return "} else " + NL
    def makeStatementElseIf(self,condition):
        return "} else if ("+condition+") {" + NL
    def makeStatementEndIf(self):
        return "} // if" + NL
    def makeStatementStartCase(self,varName):
        return "switch ("+varname+") {" + NL
    def makeStatementCase(self,constVal):
        return "case "+constVal+":" + NL
    def makeStatementEndCase(self):
        return "} // switch" + NL
    # assignments
    def makeStatementDoAssignment(self,varName,assignTo,varType = ""):
        if varType != "": return varType + " " + varName + " = ("+varType+")"+ assignTo +";" + NL
        return varName + " = "+ assignTo +";" + NL
    def makeStatementIncrement(self,varName,incVal):
        if incVal == "1": return varName + "++;" + NL
        return varName + " += "+ incVal +";" + NL
    # variable declarations
    def makeConsVarDecl(self,varType,varName,varVal):
        #varType = self.lookupType(varType)
        return "const " + varType + " "+varName  + " = (const "+varType+") ("+varVal+  ");"
    def makeVarDecl(self,varType,varName,arrayLen = None):
        return genVarOnlyDecl(self,varType,varName,arrayLen = None)
    def genVarOnlyDecl(self,varType,varName,arrayLen = None):
        ''' Generate variable declaration of the given type.
            If an array size given it will be declared as an
            array of that size.
            Note that arrayLen of 0 is handeled differently:
            it will be declared as an array argument using "[]"
            as normally used in function arguments.
        '''
        tt = self.lookupType(varType)
        s  = tt + " "+varName
        if arrayLen is not None:
            if arrayLen == 0:
                s += "[]"
            else:
                s += "[" + str(arrayLen) +"]"
        return s
    # TODO: stillused??
    def isMessage(self,structt):
        if self.findInConstList( structt,"MSG_ID"): return True
        if self.findInConstList( structt,"MSG_COND"): return True
        return False
    def isStruct(self,structt):
        if self.findInConstList( structt,"STRUCT"): return True
        return False
    def isInheritedFrom(self,structt,parnt):
        retparnt =  self.getParent(structt) 
        if retparnt == None: return False
        if structt["name"] == "StoreRecVer":
            print("fsdg")    
        if retparnt == parnt: return True
        return self.isInheritedFrom(retparnt,parnt)
    def genFileHeader(self,fileName):
        s  = self.COMMENT_START
        s += "\n  Autogenerated code by "+self.__class__.__name__+"\n"
        s += "  File name : "+fileName + "\n"
        if "name" in annotateDict:
            s += "  Name      : "+cleanstr(annotateDict["name"]) + "\n"
        if "version" in annotateDict:
            s += "  Version   : " + cleanstr(annotateDict["version"]) + "\n"
        if "copyright" in annotateDict:
            s += "  Copyright : " + cleanstr(annotateDict["copyright"]) + "\n"
        return s + self.COMMENT_END + "\n"
    # def genPackVar(self,buff,buffpos,vartype,varname, varval):
    #     #if vartype in self.typeTable2:
    #     #    return self.funcPackNamePrefix+vartype+"("+buff+", "+buffpos+", "+varname+","+varval+");"
    #     return self.funcPackCallPrefix+vartype+"("+buff+", "+buffpos+", "+varname+","+varval+");"
    def genArg(self,fields):
        s = ""
        for f in fields:
            s = s + f["name"] + ","
        return s[:-1]
    # TODO: remove? not used anymore    
    def genTypedArg(self,fields):
        s = ""
        for f in fields:
            # if the field is in the locked list it is internally
            # generated and should not be in the argument list so skip it
            if f["type"] in self.typeTableLoc:
                continue
            if "arrLen" in f:
                # 0 will force it to use [] in declaration
                arrLen = 0
            else:
                arrLen = None
            #arrLen = f.get("arrLen",None)
            s += self.genVarOnlyDecl(f["type"] , f["name"],arrLen ) + ","
            #s = s + self.lookupType(f["type"])+ "  "+ f["name"] + ","
        return s[:-1]
    #####-------------------------------------
    def makeFuncDeclr(self,structt,retType,funName,args,isSingletonFun = False,isPrivate = False):
        """ Make a valid function declaration

        Arg:
          funName    - the base name (root) of the name. If it is part of a class
                       it should be prefixed wih the class name (or what ever the
                       programming language require)
          args       - function arguments
          isSingletonFun  specifies whether it is a static class member
          isPrivate       means that it will not be called externally
        """
        s = ""
        if isPrivate:      s+= self.privateFuncDecl
        if isSingletonFun: s+= self.singletonFuncDecl
        #nn = structt['name'].upper() + "_" + funName
        nn = convertCamelToSnake(structt['name']) + "_" + funName
        s += retType + " " + nn + "(" + args + ")"
        return s
    def makeDeclUserAferUnpackFunArgList(self,structt):
        return self.makeFieldListGeneric(structt,True, seperator = "," , inclParent = True , inclConst = True, inclPrivate = False, setInitValue = False )
    def makeCallUserAferUnpackFunArgList(self,structt):
        return self.makeFieldListGeneric(structt,False, seperator = "," , inclParent = True , inclConst = True, inclPrivate = False )
    def makeDeclFunArgList(self,structt):
        return self.makeFieldListGeneric(structt,True, seperator = "," , inclParent = True , inclConst = False, inclPrivate = False )
    def makeCallFunArgList(self,structt):
        return self.makeFieldListGeneric(structt,False, seperator = "," , inclParent = True , inclConst = False, inclPrivate = False )
    def makeDeclFunArgListAll(self,structt):
        return self.makeFieldListGeneric(structt,True, seperator = "," , inclParent = True , inclConst = True, setInitValue = False )
    def makeCallFunArgListAll(self,structt):
        return self.makeFieldListGeneric(structt,False, seperator = "," , inclParent = True , inclConst = True )
    def makeClassMemberList(self,structt):
        return self.makeFieldListGeneric(structt,True, seperator = ";\n  " , inclParent = False , inclConst = True )
    def makeFieldListGeneric(self,structt,inclTypes, seperator = "," , inclParent = True , inclConst = False, noLastSeparater = True, inclArrLen =False, inclPrivate = False,setInitValue = True,namePrefix = "" ):
        """ Flexible generation of variable/argument list
        """
        fields    = structt["body"]
        s = ""
        if inclParent and "parentName" in structt:
            parnt = self.getParent(structt)
            s += self.makeFieldListGeneric(parnt,inclTypes, seperator, inclParent , inclConst,inclArrLen = inclArrLen,setInitValue =setInitValue) + seperator
        for f in fields:
            # if the field is assigned a value
            # do not include in the argument list
            # but can be over ridden with  inclConst
            if "value" in f and inclConst == False:
                continue
            if inclPrivate == False and f["name"].startswith("__"):
                continue
            arrLen = f.get("arrLen",None)
            if inclTypes:
                #s += self.genVarDecl(f["type"] , f["name"],arrLen ) + seperator
                s += self.genVarDecl(f, inclArrLen = inclArrLen, termstr = seperator,setInitValue = setInitValue )
            else:
                #s += self.genVarOnlyDecl(f["type"] , f["name"],arrLen ) + seperator
                s += namePrefix + f["name"] + seperator
        if len(s) > 2:
          if  s[0] == ',':
            s = s[1:]
        if  noLastSeparater:
            return s[:-len(seperator)]
        return s
    def genPackLenCalc(self,structt):
        # calculate the struct size by finding the last field offset
        lastField = structt["body"][-1]
        return self.genPackFieldPosCalc(structt,lastField,True)
    ## lookup if the sring is in the list
    def findInConstList(self,lst,sstr):
        for itm in lst:
            if itm['name'] == sstr: return itm
        return None
    ## try to simplify the equation if it only contains numbers
    def simplifyConstsInEq(self,eqs):
        if any(c not in '0123456789+*- ' for c in eqs):
            return eqs,False
        ll = eval(eqs)
        return str(ll),True
    # this will search hirarchically to find a constant if defined
    # first look as this structures constants
    # second look at parents constants
    # last look at global annotations
    def lookupConst(self,structt,annotag):
        if "localConst" in structt:
            lst     = structt["localConst"]
            anno = self.findInConstList(lst,annotag)
            if anno:
                if "value" in anno:
                    return anno['value']
                if "string" in anno:
                    return anno['string']
                if "expr" in anno:
                    return anno['expr']
        if "parentName" in structt:
            parnt = structt["parentName"]
            parStruct = structDict[parnt]
            aa = self.lookupConst(parStruct,annotag)
            if aa is not None: return aa
        # TODO: must still split the annotation explisitly
        #  not sure that this is the best approach
        if annotag in annotateDict: return annotagDict[annotag]
        return None

#        for itm in lst:
#            if itm['name'] == sstr: return itm
    def replaceSymbsInFieldAssgn(self,structt,expr):
        # replace predefined symbols
        if expr == "$BUFF$":
            return self.BUF_NAME
            #rs.append(self.BUF_NAME)
        elif expr == "$INDX$":
            return self.BUF_POS_NAME
            #rs.append(self.BUF_POS_NAME)
        elif expr == "$TIME_UNIX$":
            # TODO: get real time and convert to unix time
            rs.append("99999999")
        # if start with "$$" find byte offset position
        # of field in buff at end of this field
        elif expr[0] == "$" and expr[1] == "$":
            s1,pres = self.genPackFieldPosCalc(structt,expr[2:],includeField=True)
            if pres:
                s1,__  = self.simplifyConstsInEq(s1)
                return s1
        # if start with "$" find byte offset position
        # of field in buff at start of this field
        elif expr[0] == "$":  # needs buffpos/address of the field
            s1,pres = self.genPackFieldPosCalc(structt,expr[1:],includeField=False)
            if pres: return s1

        # TODO: old stuff - remove
        if expr[0] == "&":  # needs buffpos/address of the field
            s1,pres = self.genPackFieldPosCalc(structt,expr[1:],includeField=False)
            if pres: return s1
        if expr[-1] == "&":  # needs position in buff including this field
            s1,pres = self.genPackFieldPosCalc(structt,expr[:-1],includeField=True)
            if pres:
                s1,__  = self.simplifyConstsInEq(s1)
                return s1
        return expr
    # TODO: still used ???
    def findParentFieldsUsingConsts(self,structt):
        """
          Parents can assign constant symbols
          to fields. Child classes can redefine these values
          in which cases these fields should re-assign the values.
          This function will determine if such constans are
          used in the parent fields.

          @return a list of fields that are re-assigned
        """
        fieldlist = []
        parnt = self.getParent(structt)
        if parnt == None: return fieldlist
        for fld in parnt["body"]:
            if "value" in fld:
                splt = self.splitAssigmentEquation(fld["value"])
                # look a each symbol and replace known values
                for elm in splt:
                    # just skip if empty
                    if elm == "": continue
                    # see if in symbol list
                    sym = self.lookupConst(structt,elm)
                    if sym is not None:
                        fieldlist.append(fld)
                        break # at least one present so stop looking
        return fieldlist
    def findFieldName(self,structt,fieldname):
        """
          Find a field name in the class hierarchy.

          @return message struct and field
          @return (None,None) if not found
        """
        for fld in structt["body"]:
            if fld["name"] == fieldname:
                return (structt,fld)
        parnt = self.getParent(structt)
        if parnt != None: 
            return self.findFieldName(parnt,fieldname)
        return (None,None)

    #assignemtSplitSymbols = r"([\[\]\+\-\*/\)\(\=\>\<\!)])"
    # have a single split function to split the assignments consitently
    def splitAssigmentEquation(self,eqs):
        return re.split(r"([\[\]\+\-\*/\)\(\=\>\<\!)])",eqs)
    def simplifyAssignment(self,structt,eqs):
        # first handle it if it is a string
        if eqs[0] == '"': return eqs
        # replace if a single replacement
        sym = self.lookupConst(structt,eqs)
        if sym is not None: return sym
        # TODO: partial replacement, e.g. CONST_LEN+2
        #sl = re.split(r"([\[\]\+\-\*/\)\(\=\>\<\!)])",eqs)
        splt = self.splitAssigmentEquation(eqs)
        rs = []
        # look a each symbol and replace known values
        for elm in splt:
            # just skip if empty
            if elm == "": continue
            # see if in symbol list and replace
            sym = self.lookupConst(structt,elm)
            if sym is not None:
                rs.append(sym)
            # special reserved symbols start with $
            elif elm[0] == "$":
                pp = self.replaceSymbsInFieldAssgn(structt,elm)
                rs.append(pp)
            # else just keep the original
            else:
                rs.append(elm)
        return "".join(rs)
        # TODO: function call replacements e.g. CRC()
        return eqs
    def genVarDecl(self,sfield,inclArrLen =False, termstr = ";", setInitValue = True):
        if not "type" in sfield:
            return ""
        vtype = sfield["type"]
        vname = sfield["name"]
        if vtype in self.typeTableEnum:
            s = "enum "+ sfield["enumName"] +" "+vname
        elif vtype in self.typeTable2:
            s = self.lookupType(vtype) +" "+vname+"["+sfield["type"]+"]"
        else:
            s = self.lookupType(vtype) +" "+vname
        if 'arrLen' in sfield:
            if inclArrLen:
                s = s +"["+sfield['arrLen']+"]"
            else:
                s = s +"[]"
        if 'value' in sfield:
            if setInitValue:
                s = s + " = " +  sfield["value"]
        s = s + termstr
        return s
    def isAnnoTagDefined(self,structt,annostr):
        """Check if the annotation is present for this structure
        """
        if "anno" in structt:
                lst     = structt["anno"]
                anno = self.findInConstList(lst,annostr)
                if anno: return True
        return False

    def getLocalAnno(self,structt,annotag,default=None):
        """Find the relevant annotation if available otherwise assume a default.

           More or less the following logic:
              - look at struct annotation for this language
              - look at struct annotation
              - look at global language annotation
              - look at global annottion
        """
        # this is the language spesific annotation tag that will overrride the general annotation
        lannotag = self.LANG +"_"+annotag
        # local struct annotation takes precedence, so first look at that
        if "anno" in structt:
            lst     = structt["anno"]
            anno = self.findInConstList(lst,lannotag)
            if anno:
                if "value" in anno:
                    return anno['value']
                if "string" in anno:
                    return cleanstr(anno['string'])
                    #return anno['string']
            anno = self.findInConstList(lst,annotag)
#            if anno: return anno['value']
            if anno:
                if "value" in anno:
                    return anno['value']
                if "string" in anno:
                    return cleanstr(anno['string'])
        lst     = annotateDict
        if lannotag in lst:
            return lst[lannotag]
        if annotag in lst:
            return lst[annotag]
        return default

# ==================================================

class CcodeGenerator(BaseCodeGenerator):
    FUNCTION_BLOCK_START = "\n{\n"
    FUNCTION_BLOCK_END   = "} // end\n"
    
    def __init__(self):
        # set the language
        self.LANG = BaseCodeGenerator.LANG_C
    def simpleReindent(self,ccode):
        lines = ccode.splitlines()
        # must still handle special cases for comments
        lines = [s.strip() for s in lines]
        ss = ""
        indent = ""
        for ll in lines:
            if "{" in ll and "}" in ll : # special case with {} in same line
                ss += indent + ll + "\n"
                continue
            if "}" in ll: indent  = indent[:-4]
            ss += indent + ll + "\n"
            if "{" in ll: indent += "    "
        return ss
    def makeCondError(self,condi,msg=""):
        return "if ("+condi+") return ret;\n"
    def makeStatementDeclareVar(self,varName,varType,assignedVal=""):
        if assignedVal == "": return varType +" "+ varName +";" + NL
        return varName + " = ("+ varType +") ("+ assignedVal +");"  + NL    
    # unpack a variable from the byte array
    def makeUnpackVarFromByteArr(self,byteArrName,arrIndx,varType,varByteLen,varName,indent, incTypeDecl= True, littleEndian = True):
        varTypeI = "uint32_t"
        s  = indent + varType + " " if incTypeDecl  else indent   
        s += varName + " = "
        s += "("+varTypeI+") ("+byteArrName+"["+arrIndx+"])"
        for i in range(1,varByteLen):
           s += "+ ("+varTypeI+") ("+byteArrName+"["+arrIndx+"+"+str(i)+"])"
        s += ";\n"
        return s
    # def oldmakeUnpackVar32FromByteArr(self,byteArrName,arrIndx,varType,varName,indent, incTypeDecl= True, littleEndian = True):
    #     return self.makeUnpackVarFromByteArr(byteArrName,arrIndx,varType,4,varName,indent, incTypeDecl, littleEndian)
    
        # varTypeI = "uint32_t"
        # s  = indent + varType + " " if incTypeDecl  else indent   
        # s += varName + " = "
        # s += "("+varTypeI+") ("+byteArrName+"["+arrIndx+"]) +"
        # s += "("+varTypeI+") ("+byteArrName+"["+arrIndx+"+1]) +"
        # s += "("+varTypeI+") ("+byteArrName+"["+arrIndx+"+2]) +"
        # s += "("+varTypeI+") ("+byteArrName+"["+arrIndx+"+3])"
        # s += ";\n"
        # return s
    def makeAssignByteToArr(self,byteArrName,arrIndx,byteVal):
        return byteArrName + "[" + arrIndx +"] = (uint8_t)("+byteVal+");\n"
    def genPackFieldCode(self,f,indent="",deref = ""):
        # if deref is None:
        #     preamble = self.lookupFieldType(f)+"  "
        # else:
        #     preamble = deref
        tsize = self.lookupTypeSize(f["type"])
        if "arrLen" in f:
            # if it is a structure array unpack in a loop
            if f["type"] in structDict:
                s  = "int ii;\n"
                #s += self.lookupFieldType(f) + "  " + f["name"] +"[" + f["arrLen"] + "];\n"
                s += "    for (ii = 0; ii < "+ f["arrLen"] +" ;ii++) {\n"
                args = "&"+f["name"]+"[ii],&buff["+self.BUF_POS_NAME+"],bufSize-"+self.BUF_POS_NAME+""
                #s += "  int ret = "+f["type"]+self.funcPackInBuff+"("+f["name"]+"[ii],&buff["+self.BUF_POS_NAME+"],bufSize-"+self.BUF_POS_NAME+");\n"
                #if f["type"] in structDict:
                s += "      int ret = "+self.makePackInStructDecl(structDict[f["type"]],args) +";\n"
                #s += "      if (ret < 0) return ret;\n"
                s += "      " + self.makeCondError("ret<0")
                s += "      "+self.BUF_POS_NAME+" += ret;\n"
                s += "    } // for ii\n"
                return s
            lenstr = str(tsize)+"*" + f["arrLen"]
            # TODO: Maybe would be better have a makeArrayDecl
            s = "// just copy but no valid if endianess differ\n"
            #self.lookupFieldType(f) + "  " + f["name"] +"[" + f["arrLen"] + "];\n"
            ## this is only valid if the endianess is the same for multibyte types !!!!
            s += "    memcpy("+self.BUF_NAME+"+"+self.BUF_POS_NAME+","+f["name"] +","+lenstr + ");\n"
            s += "    "+self.BUF_POS_NAME+" += "+ lenstr + ";\n"
            return s
        if tsize == 1:
            s  = indent + self.makeAssignByteToArr(self.BUF_NAME,""+self.BUF_POS_NAME,deref+f["name"])
            #s += IND1 + self.BUF_POS_NAME+"++" + NL
            s += indent + self.makeStatementIncrement(self.BUF_POS_NAME,"1") 
            return s
            #return self.makeAssignByteToArr(self.BUF_NAME,""+self.BUF_POS_NAME+"++",deref+f["name"])
            #return self.BUF_NAME+"["+self.BUF_POS_NAME+"++] = (uint8_t)"+deref+ f["name"] + ";\n"
            #return self.BUF_NAME+"["+self.BUF_POS_NAME+"++] = (uint8_t)"+ f["name"] + ";\n"
        elif tsize == 2:
            # this is stored in little endian
            s  = indent +self.makeAssignByteToArr(self.BUF_NAME,""+self.BUF_POS_NAME,deref+f["name"])
            s += indent +self.makeAssignByteToArr(self.BUF_NAME,""+self.BUF_POS_NAME+"+1",deref+f["name"]+ ">>8")
            #s += NL + self.BUF_POS_NAME+"+=2" 
            s += indent + self.makeStatementIncrement(self.BUF_POS_NAME,"2")
            #s += NL
            return s
            #s = self.BUF_NAME+"["+self.BUF_POS_NAME+"++] = (uint8_t)"+deref+ f["name"] + ";\n"
            #return s + "    "+ self.BUF_NAME+"["+self.BUF_POS_NAME+"++] = (uint8_t)("+deref+ f["name"] + ">>8);\n"
        elif tsize == 4:
            # this is stored in little endian
            s  = indent +self.makeAssignByteToArr(self.BUF_NAME,""+self.BUF_POS_NAME,deref+f["name"])
            s += indent +self.makeAssignByteToArr(self.BUF_NAME,""+self.BUF_POS_NAME+"+1",deref+f["name"]+ ">>8")
            s += indent +self.makeAssignByteToArr(self.BUF_NAME,""+self.BUF_POS_NAME+"+2",deref+f["name"]+ ">>16")
            s += indent +self.makeAssignByteToArr(self.BUF_NAME,""+self.BUF_POS_NAME+"+3",deref+f["name"]+ ">>24")
            #s += NL + self.BUF_POS_NAME+"+=4"
            s += indent + self.makeStatementIncrement(self.BUF_POS_NAME,"4")
            #s += NL
            return s
            #s  = self.makeLineComment(" it is faster to copy byte by byte than calling memcpy()")
            #s += "    "+ self.BUF_NAME+"["+self.BUF_POS_NAME+"++] = (uint8_t)"+deref+ f["name"] + ";\n"
            #s += "    "+ self.BUF_NAME+"["+self.BUF_POS_NAME+"++] = (uint8_t)("+deref+ f["name"] + ">>8);\n"
            #s += "    "+ self.BUF_NAME+"["+self.BUF_POS_NAME+"++] = (uint8_t)("+deref+ f["name"] + ">>16);\n"
            #return s + "    "+ self.BUF_NAME+"["+self.BUF_POS_NAME+"++] = (uint8_t)("+deref+ f["name"] + ">>24);\n"
        # if it is a structure array pack in a loop
        if f["type"] in structDict:
            # s  = "int ii;\n"
            # s += "for (ii = 0; ii < "+ f["arrLen"] +" ;ii++) {\n"
            args = "&"+f["name"]+",&buff["+self.BUF_POS_NAME+"],bufSize-"+self.BUF_POS_NAME+""
            #if f["type"] in structDict:
            s  = "{ // start block\n"
            s += "      int ret = "+self.makePackInStructDecl(structDict[f["type"]],args) +";\n"
            s += "      if (ret < 0) return ret;\n"
            s += "      "+self.BUF_POS_NAME+" += ret;\n"
            s += "    } // end block\n"
            return s
        s  = ""+self.BUF_POS_NAME+"    += " + self.funcPackCallPrefix + f["type"].capitalize()
        s += "(" +self.BUF_NAME +", "+self.BUF_POS_NAME+","+ f["name"] + ");\n"
        return s
    def genUnpackFieldCode(self,f,deref = None):
        """
        Generate code to unpack the the data from the byte array into
        a variable

        f       is the field
        deref   indicate if the variable should  be dereferenced like in the case oof a class
        """
        #if f['type'] == "enum8":
        #    print ("ffffffffg")
        if deref is None:
            preamble = "" #self.lookupFieldType(f)+"  "
        else:
            preamble = deref
        tsize   = self.lookupTypeSize(f["type"])
        bufname = self.BUF_NAME
        s = ''
        if "arrLen" in f:
            # if it is a structure array unpack in a loop
            if f["type"] in structDict:
                s  = "int ii;\n"
                s += self.lookupFieldType(f) + "  " + f["name"] +"[" + f["arrLen"] + "];\n"
                s += "for (ii = 0; ii < "+ f["arrLen"] +" ;ii++) {\n"
                #s += "  int ret = "+f["type"]+self.funcUnpackInStruct+"("+f["name"]+"[ii],&buff["+self.BUF_POS_NAME+"],buflen-"+self.BUF_POS_NAME+");\n"
                s += "  int ret = "+convertCamelToSnake( f["type"])+"_"+self.funcUnpackBaseName+"(&"+f["name"]+"[ii],&buff["+self.BUF_POS_NAME+"],buflen-"+self.BUF_POS_NAME+");\n"
                s += "  if (ret < 0) return ret;\n"
                s += "  "+self.BUF_POS_NAME+" += ret;\n"
                s += "} // for ii\n"
                return s
            # normally an array or a base type
            #  - else unknown type and will give a compile error
            lenstr = str(tsize)+"*" + f["arrLen"]
            # type declaration
            # TODO: Maybe would be better have a makeArrayDecl
            if deref is None:
                s = self.lookupFieldType(f) + "  " + f["name"] +"[" + f["arrLen"] + "];\n"
            # TODO: this is only valid if the endianess is the same for multibyte types !!!!
            s += "memcpy("+f["name"] +","+bufname+"+"+self.BUF_POS_NAME+","+lenstr + ");\n"
            return s + "pos += "+lenstr+";\n"
        if tsize == 1:
            #return  preamble+ f["name"] + " = ("+self.lookupFieldType(f)+ ")" + bufname+"["+self.BUF_POS_NAME+"++] " + ";\n"
            varName  = preamble+ f["name"]
            assignTo = bufname+"["+self.BUF_POS_NAME+"] "
            s  =  self.makeStatementDoAssignment(varName,assignTo,varType = self.lookupFieldType(f))
            #s +=  self.makeStatementDoAssignment(preamble+ f["name"],assignTo,varType = self.lookupFieldType(f))
            s += self.makeStatementIncrement(self.BUF_POS_NAME,"1")
            return s
        elif tsize == 2:

            varName  = preamble+ f["name"]
            # this is  little endian
            s += self.makeUnpackVarFromByteArr(self.BUF_NAME,self.BUF_POS_NAME,self.lookupFieldType(f),tsize,varName,indent="", incTypeDecl= True, littleEndian = True)

            #return  preamble +f["name"] + " = ("+ self.lookupFieldType(f)+ ")(" + bufname+"["+self.BUF_POS_NAME+"] + ("  + bufname+"["+self.BUF_POS_NAME+"+1]<<8)" +  ");\n"+self.BUF_POS_NAME+" +=2;\n"
            #return  self.lookupType(f["type"])+"  "+f["name"] + " = ("+ self.lookupType(f["type"])+ ")(" + bufname+"["+self.BUF_POS_NAME+"] + ("  + bufname+"["+self.BUF_POS_NAME+"+1]<<8)" +  ");\n"+self.BUF_POS_NAME+" +=2;\n"
            s += self.makeStatementIncrement(self.BUF_POS_NAME,"2")
            return s
        elif tsize == 4:
            # this is  little endian
            #return  preamble +f["name"] + " = ("+ self.lookupFieldType(f)+ ")(" + bufname+"["+self.BUF_POS_NAME+"] + ("  + bufname+"["+self.BUF_POS_NAME+"+1]<<8) + (((uint32_t)"+ bufname+"["+self.BUF_POS_NAME+"+2])<<16) + (((uint32_t)"+ bufname+"["+self.BUF_POS_NAME+"+3])<<24)) " +  ";\n"+self.BUF_POS_NAME+" +=4;\n"
            #return  self.lookupType(f["type"])+"  "+f["name"] + " = ("+ self.lookupType(f["type"])+ ")(" + bufname+"["+self.BUF_POS_NAME+"] + ("  + bufname+"["+self.BUF_POS_NAME+"+1]<<8) + (((uint32_t)"+ bufname+"["+self.BUF_POS_NAME+"+2])<<16) + (((uint32_t)"+ bufname+"["+self.BUF_POS_NAME+"+3])<<24)) " +  ";\n"+self.BUF_POS_NAME+" +=4;\n"
            varName  = preamble+ f["name"]
            #assignTo = bufname+"["+self.BUF_POS_NAME+"] "
            #makeUnpackVarFromByteArr(self,byteArrName,arrIndx,varType,varByteLen,varName,indent, incTypeDecl= True, littleEndian = True)
            s += self.makeUnpackVarFromByteArr(self.BUF_NAME,self.BUF_POS_NAME,self.lookupFieldType(f),tsize,varName,indent="", incTypeDecl= True, littleEndian = True)

            #??
            #s  =  self.makeStatementDoAssignment(varName,assignTo,varType = self.lookupFieldType(f))
            s += self.makeStatementIncrement(self.BUF_POS_NAME,"4")
            return s
        # if we get here it is probably a single struct
        s  = f["type"] + self.TYPE_POSTFIX + "  " + f["name"] + ";\n"

        if f["type"] in structDict:
            s += "// Read a structured type\n"
            structt = structDict[f["type"]]
            args    = "&"+f["name"] +",buff,"+self.BUF_POS_NAME+""
            tmpvar  = f["name"]+"_ret"
            s += self.INT_TYPE_NAME +" " +tmpvar + " = " + self.makeUnpackFunCall(structt,args,namePrefix = "") + ";\n"
            s += "if (" +tmpvar+" <0) return "+tmpvar+";\n"
            s += ""+self.BUF_POS_NAME+" += " + tmpvar +";\n"
        else:
            s += "// Unknown type found - generate a skeleton \n"
            s += ""+self.BUF_POS_NAME+"    += " + self.makeFuncDeclr(f,"",self.funcUnpackInStruct,"???") +";\n"

        #s += ""+self.BUF_POS_NAME+"    += " + self.funcPackCallPrefix + f["type"].capitalize()
        #s += "(" +self.BUF_NAME +", "+self.BUF_POS_NAME+","+ f["name"] + ");\n"
        return s
    def genPackFields(self,structt,structPrefix = ""):
        s = ""
        fields    = structt["body"]
        for f in fields:
            # TODO - remove
            if f["type"] == "CRC16":
              p1,fnd1 = self.genPackFieldPosCalc(structt,f['rangeStart'],False)
              p2,fnd2 = self.genPackFieldPosCalc(structt,f['rangeEnd'],True)
              s += "    int16_t   "+f["name"] +" += CalcCRC16"+f["name"]+"("+self.BUF_NAME+", "+p1 + "," + p2 + ");\n"
            #-- check if it is assigned a value
            if "retags" in f:
                s += "    "+self.makeLineComment(" retag field")
            if "value" in f:
                s += "    "+self.makeLineComment(" this is a fixed assigned field")
                s += "    " + self.makeConsVarDecl(self.lookupFieldType(f) ,f["name"],f["value"]) +"\n"
            # TODO - remove
            if "callName" in f :
                #if "annoCheckVal" in f:   # should the constant value be checked
                    #s1 += self.makeLineComment(" call function to calc value")
                    arg1 = self.replaceSymbsInFieldAssgn(structt,f["arg1"]);
                    arg2 = self.replaceSymbsInFieldAssgn(structt,f["arg2"]);
                    s += IND1 + self.makeLineComment("call user function with "+f["arg1"]+" and " +f["arg2"])
                    funCallStr = f["callName"]+"(" +self.BUF_NAME +","+arg1+","+arg2 +")"
                    s += IND1 +self.makeConsVarDecl(self.lookupFieldType(f),f["name"],funCallStr) +"\n"
            s +=  self.genPackFieldCode(f,indent=IND1,deref=structPrefix)
            #s += "    "+self.BUF_POS_NAME+"    += " + self.funcPackNamePrefix + f["type"].capitalize()
            #s += "(" +self.BUF_NAME +", "+self.BUF_POS_NAME+","+ f["name"] + ");\n"
        return s
    def makeFunNameUserAferUnpack(self,structt):
        fname = self.getLocalAnno(structt,"call_after_unpack","TRUE")
        if fname  == "FALSE": return None
        if fname  != "TRUE": return fname
        fnamepre  = convertCamelToSnake(structt["name"])
        fnamepre += self.FUN_DEFAULT_AFTER_UNPACK_CALL_POSTFIX
        funname   = self.getLocalAnno(structt,"call_after_unpack",default = fnamepre)
        funname   = cleanstr(funname)
        return funname

    def genUnpackFields(self,structt,stuctderef = None,indent = "    "):
        if self.isStructDeclUsed(structt):
            stuctderef = "this->"
        # else:
        #     stuctderef = None
        fields    = structt["body"]
        s = ""
        s1  = indent
        #userfunargs = ""
        for f in fields:
            #s1 += self.genUnpackFieldCode(f,deref = stuctderef)
            #userfunargs += f["name"] + ","
            # check if it is assigned a value
            if "value" in f :
                if "annoCheckVal" in f:   # should the constant value be checked
                    s1 += self.makeLineComment(" check the field value is equal to the expected value")
                    #s1 += self.makeConsVarDecl(f["type"],"_"+f["name"],f["value"]) +"\n"
                    s1 += "if ("+f["name"] +" != "+f["value"] +") return "+self.CONST_ERR_NOT_EQ+ ";" +"\n"
                else:
                    s1 += self.makeLineComment(" this is an assigned value but not verified here")
                    #s1 += self.makeConsVarDecl(f["type"],f["name"],f["value"]) +"\n"
            # TODO: probably replace by more generic call so remove
            # the field is assigned to a functions call
            if "callName" in f :
                if "annoCheckVal" in f:   # should the constant value be checked
                    s1 += self.makeLineComment(" check the field value is equal to the returned function value")
                    arg1 = self.replaceSymbsInFieldAssgn(structt,f["arg1"]);
                    arg2 = self.replaceSymbsInFieldAssgn(structt,f["arg2"]);
                    s1 += self.makeLineComment("call user function with "+f["arg1"]+" and " +f["arg2"])
                    funCallStr = f["callName"]+"(" +self.BUF_NAME +","+arg1+","+arg2 +")"
                    #s1 += self.lookupType(varType) + " " + "ret_"+f["name"] + " = " +
                    s1 += self.makeConsVarDecl(self.lookupFieldType(f),"ret_"+f["name"],funCallStr) +"\n"
                    s1 += "if ("+ "ret_"+f["name"] +" != "+f["name"] +") return "+self.CONST_ERR_NOT_EQ+ ";" +"\n"
            #s += "(" +self.BUF_NAME +", "+self.BUF_POS_NAME+","+ f["name"] + ");\n"
            s1 += self.genUnpackFieldCode(f,deref = stuctderef)
        # fixup the indentation
        s += s1.replace("\n","\n"+indent)
        # remove last comma
        #cpos = userfunargs.rfind(",")
        #if cpos > 1: userfunargs = userfunargs[:cpos]
        if self.getLocalAnno(structt,"call_after_unpack","TRUE") != "FALSE":
            # call the user function
            funname     = self.makeFunNameUserAferUnpack(structt)
            userfunargs = self.makeCallUserAferUnpackFunArgList(structt)
            s += self.makeLineComment('call user function with data')
            s += indent + funname + "(" +userfunargs+")" + self.STATEMENT_END
            #s += self.makeFunctionCall(funname,userfunargs)
        return s
    # includeField indicate if the field named fieldName should be included in the "+self.BUF_POS_NAME+"ition calculations
    def genPackFieldPosCalc(self,structt,fieldName,includeField):
        fields    = structt["body"]
        s = ""
        if "parentName" in structt:
            parnt = structt["parentName"]
            parStruct = structDict[parnt]
            s,fieldFound = self.genPackFieldPosCalc(parStruct,fieldName,includeField)
            if fieldFound: return s,True
            if s != "":  s += "+ "
        for f in fields:
            # if the selected field size must be included
            if includeField == False:
                if f["name"] == fieldName:
                    if len(s) < 1: return "0",True  # if it is the first element
                    return s[:-1],True
            # if it is an array, multiply with the array length
            if 'arrLen' in f:
                s = s +" ("+f['arrLen']+")*"
            # get the size of the element
            s += str(self.lookupTypeSize(f["type"]))+ "+"
            if f["name"] == fieldName:
                return s[:-1],True
        return s[:-1],False
    # TODO: not ued ?
    def genCreateFunDecl(self,structt,copyToBuff=True,msgIdArg = False,namePrefix = ""):
        args  = "void"
        s     = self.makeFuncDeclr(structt,structt['name'],namePrefix+self.funcCreateName,args)
        return s
    def genMsgFactoryFromBuffFunDecl(self,structt,retType=False,msgIdArg = False,namePrefix = ""):
        args  = self.buffArrType+ " " +self.BUF_NAME+"[],"+self.INT_TYPE_NAME +" buflen "
        if retType == False: retType = structt['name']
        s     = self.makeFuncDeclr(structt,retType,namePrefix+self.funcCreateFromBufName,args, isSingletonFun = True)
        return s
    # make the funtion declaration for the function that takes a byte array 
    # as argument, determine the message in the array and extract the 
    # message data from the array   
    def makeUnpackAllFunDecl(self,baseStructt,namePrefix = ""):
        args  = self.BUF_FUN_ARG + ","+self.BUF_LEN_ARG
        s     = self.makeFuncDeclr(baseStructt,self.LEN_TYPE_NAME,namePrefix+self.FUN_UNPACK_ALL_NAME,args)
        return s
    def makeUnpackFunCall(self,structt,args,namePrefix = ""):
        return self.makeFuncDeclr(structt,"",namePrefix+self.funcUnpackBaseName,args)
    def genUnpackFunDecl(self,structt,copyToBuff=True,classLike=False,namePrefix = ""):
        s  = "\n"
        # generate the comments
        s += self.COMMENT_START
        s += self.doxPre + "param buff[]    buffer with data to be unpacked "
        s += self.doxPre + "param buflen    number of bytes in buff, must be at long enough for complete struct "
        s += self.doxPre + "return if > 0 : position in array of last extracted data"
        s += self.doxPre + "return if < 0 : error in data stream (-4: too short, -23: CRC error"
        s += "\n" + self.COMMENT_END +"\n"
        args  = "uint8_t  "+self.BUF_NAME+"[],"+self.INT_TYPE_NAME +" buflen "
        #if classLike:
        if self.isStructDeclUsed(structt):
            args = structt["name"]+self.TYPE_POSTFIX +"* this,"+args
        s += self.makeFuncDeclr(structt,self.INT_TYPE_NAME,namePrefix+self.funcUnpackBaseName,args)
        return s
    def genPackFunDecl(self,structt,copyToBuff=True,msgIdArg = False,namePrefix = ""):
        structnme     = structt["name"]
        fields        = structt["body"]
        parentArgs    = ""
        s  = "\n"
        # generate the comments
        s += self.COMMENT_START
        s += self.doxPre + "param buff[]"+self.doxPost+"    buffer into which data should be packed "
        s += self.doxPre + "param pos"+self.doxPost+"       start position in buffer "
        s += self.doxPre + "return if > 0"+self.doxPost+"   position in array of last extracted data"
        s += self.doxPre + "return if < 0"+self.doxPost+"   error in data stream "
        s += "\n" + self.COMMENT_END +"\n"
        #-- build the argument list for the function
        if copyToBuff:
            #args = "uint8_t  "+self.BUF_NAME+"[],"+self.INT_TYPE_NAME +" bufSize, "
            args = self.BUF_FUN_ARG + "," + self.BUF_SIZE_ARG + ", "
        else: args = ""
        args += self.makeDeclFunArgList(structt)
        if len(args) > 2:
          if args[len(args)-2] == ',': 
            args = args[:-2] 
        #-- build the function call
        s += self.makeFuncDeclr(structt,self.INT_TYPE_NAME,namePrefix+self.funcPackBaseName,args)
        return s
        # TODO: not sure this is used
    def genPackFieldAssignments(self,structt,structPrefix = ""):
        s = ""
        if "parentName" in structt:
            # fill in the inherited data from the parent
            prn           = structt["parentName"]
            parentStruct  = structDict[prn]
            s += self.genPackFieldAssignments(parentStruct,structPrefix=structPrefix)
        # build the assignment of the data fields to the buffer
        s += self.genPackFields(structt,structPrefix = structPrefix )
        return s
    def genPackFun(self,structt,msgIdArg = False,namePrefix = ""):
        # determine if the function should be defined
        ret = self.getLocalAnno(structt,"pack","TRUE")
        if ret == "FALSE" : return ""    # function should not be generated
        # determine if a function call in pack is defined
        callFuncName          = self.getLocalAnno(structt,"call_in_pack","FALSE")
        localBuffAndUserFunct = (callFuncName != "FALSE")
        #parentArgs            = ""
        s  = self.genPackFunDecl(structt,copyToBuff= not localBuffAndUserFunct,msgIdArg = msgIdArg,namePrefix = namePrefix)
        s += "\n{\n"
        #-- generate constant declaraions for the struct local constant assignments
        locConst = structt.get("localConst",[])
        for itm in locConst:
            valexp = itm.get("value","")+itm.get("expr","")
            s1 = self.makeConsVarDecl(self.INT_TYPE_NAME,itm["name"],valexp)
            s +=  "    "+ (s1) +"\n"
        #-- calculate the length of the data
        (structLen,__) = self.genPackLenCalc(structt)
        s += "    const "+self.LEN_TYPE_NAME +" "+self.BUF_LEN_NAME+" = " + structLen
        #s += "    const "+self.INT_TYPE_NAME +" "+self.BUF_LEN_NAME+" = " + structLen
        s += ";\n"
        s += "          "+self.LEN_TYPE_NAME +" "+self.BUF_POS_NAME+"    = 0;\n"
        if localBuffAndUserFunct:
            s += "    "+self.BUF_DECLARE() + ";\n"
            #s += "    uint8_t   "+self.BUF_NAME+"["+self.BUF_LEN_NAME+"];\n"
        else:
            s += IND1 + self.makeStatementIf(self.BUF_LEN_NAME+" > bufSize")
            s += IND2 + "return "+self.CONST_ERR_BUFF_SIZE+";   "+self.makeLineComment("buffer to small")
            s += IND1 + self.makeStatementEndIf()
            #s += "    if ("+self.BUF_LEN_NAME+" > bufSize) return "+self.CONST_ERR_BUFF_SIZE+";   "+self.makeLineComment("buffer to small")
        if "retags" in structt:
            retags    = structt["retags"]
            for tag in retags:
                s += IND1 + self.makeLineComment("assign values to overwritten tags")
                # find the message/struct where the field is declared
                (ss,ff) = self.findFieldName(structt,tag["name"])
                if ss != None:
                    #parnt = self.genInternalLink(ss["name"])
                    #valstr = m.makeEnumLinkIfContains(tag["expr"])    
                    ttype = self.lookupFieldType(ff)
                    s += "    "+ ttype + " " + ff["name"] + " = " + tag["expr"] + ";\n"    
                else:  s +=  "#error Error TAG not defined:" + ss["name"]

        if "parentName" in structt:
            # fill in the inherited data from the parent
            prn           = structt["parentName"]
            parentStruct  = structDict[prn]
            s += self.genPackFields(parentStruct)
        #-- build the assignment of the data fields to the buffer
        s += self.genPackFields(structt)
        # if a funcion must be called before return
        if localBuffAndUserFunct:
            s += "    return  "+callFuncName+"("+self.BUF_NAME
            s += ", "+self.BUF_POS_NAME+");\n"
        else:
            s += "    return  "+self.BUF_POS_NAME+";\n"
        s += "\n} // end\n"
        return s

    # make a function declaration for a user function that is called
    # a the end of an unpack function. Note tis is only generated
    # if a structure is annotate to have such a function
    def makeUserAferUnpackFunDecl(self,structt,inctypedecl=False): #,callParentUnpack=False,classLike=False,namePrefix = ""):
        if not self.isStructDeclUsed(structt):
            # defaultfunname = convertCamelToSnake(structt['name']) + "_" +"userfun"
            # funname = self.getLocalAnno(structt,"call_after_unpack",default = defaultfunname)
            # funname = cleanstr(funname)
            funname = self.getLocalAnno(structt,"call_after_unpack",default = None)
            if funname == None: return "    // no call after unpack\n"
            funname = cleanstr(funname)
            funname = convertCamelToSnake(structt['name']) + "_" + funname
            if inctypedecl:
                args    = self.BUF_TYPE_NAME + " "+ self.BUF_NAME+"[]," +self.INT_TYPE_NAME + " "+self.BUF_POS_NAME+""+"," + self.makeDeclFunArgList(structt)
                return self.INT_TYPE_NAME +" " + funname + "(" +args+");\n"
            else:
                args    = self.BUF_NAME +", "+self.BUF_POS_NAME+"," + self.makeCallFunArgList(structt)
                return "    "+funname + "(" +args+");\n"
        return ""
    def makePackInStructDecl(self,structt,args = None,inctypedecl=False): #,callParentUnpack=False,classLike=False,namePrefix = ""):
        if self.isStructDeclUsed(structt):
            funname = convertCamelToSnake(structt['name']) + "_" + self.funcPackInBuff # funcCopyStruct
            if args is None:
                args  = structt["name"]+self.TYPE_POSTFIX +"* this, uint8_t buff[],int bufLen"
                return self.INT_TYPE_NAME + " " +funname+ "("+args+")\n"
            else:
                return " " +funname+ "("+args+")"
        return ""
    def genUnpackFun(self,structt,callParentUnpack=False,classLike=False,namePrefix = ""):
        # determine if the function should be defined
        ret = self.getLocalAnno(structt,"unpack","TRUE")
        if ret == "FALSE" : return ""    # function should not be generated
        s  = self.genUnpackFunDecl(structt,classLike=False,namePrefix = namePrefix)
        s += "\n{\n"
        s += "    "+self.INT_TYPE_NAME +" "+self.BUF_POS_NAME+" = 0;\n"
        if "parentName" in structt:
            prn           = structt["parentName"]
            parentStruct  = structDict[prn]
            if callParentUnpack:
                funname = prn+"::"+self.funcUnpackBaseName
                s += "    "+self.BUF_POS_NAME+" += "+self.makeFuncDeclr(structt,"",funname,args = "buff,"+self.BUF_POS_NAME+"") + ";\n"
            else:
                s += self.genUnpackFields(parentStruct)

        #-- build the assignment of the data fields to the buffer
        s += self.genUnpackFields(structt)
        # make sure that the buffer had enough data
        s += "if ( "+self.BUF_POS_NAME+" > buflen) return "+ self.CONST_ERR_BUFF_SIZE +";\n"
        # call the user function only if not implemented as struct
        if not self.isStructDeclUsed(structt):
            s += self.makeUserAferUnpackFunDecl(structt)
        s += "    return  "+self.BUF_POS_NAME+";\n"
        s += "} // end\n"
        return s

    def genEnumStart(self,enm):
        return  "\ntypedef enum "+enm["enumName"] + " {"
    def genEnumEnd(self,enm):
        return  "} "+enm["enumName"] + self.TYPE_POSTFIX +";\n"

    def genAllEnumDefs(self,separator = ","):
        s = ""
        lastSeperator = len(s)
        for enm in enumList:
            s += self.genEnumStart(enm) + "\n"
            #s += "\ntypedef enum "+enm["enumName"] + " {\n"
            elmlist = enm["values"]
            for vv in elmlist:
                s += "  "+vv["name"].ljust(20) +" = "+ vv["value"].ljust(4)+separator
                lastSeperator = len(s)
                if "comment2" in vv:
                  s += "    " + self.COMMENT_LINE  + vv["comment2"]
                  #s += "    " + self.COMMENT_LINE  + vv["comment2"]
                s += "\n"
            # remove the comma after the last enum
            if lastSeperator > 4:
                s = s[:lastSeperator-1] + ' ' + s[lastSeperator:]
            s += self.genEnumEnd(enm) + "\n"
            #s += "} "+enm["enumName"] + self.TYPE_POSTFIX +";\n"
        return s + "\n"
    # def genHeader(self,pp):
    #     s = "*Autogenerated code\n"
    #     if  "comment" in pp:
    #       s += pp["comment"]
    #     return s
    def makeUserProcessMsgDecl(self,structt):
        s  = "void "+ self.funcProcessFunPrefix + structt["name"] +"("
        s += self.makeDeclFunArgListAll(structt)
        s += ");\n"
        return s

    def genProcessMsgDetail(self,structt):
        s  = "      "+self.makeLineComment( "unpack each field into a variable")
        #s += self.genVarDecl(sfield,inclArrLen =False, termstr = ";")
        #s += "      " + self.makeFieldListGeneric(structt,inclTypes = True, seperator = ";\n      " , inclParent = False , inclConst = False,  noLastSeparater = False ,inclArrLen =True )
        s1 = "      "
        for f in  structt["body"]:
           s1 += self.genUnpackFieldCode(f)
        s  += s1.replace("\n","\n      ")
        s += "if ("+self.BUF_POS_NAME+" > buflen) "
        s += "   {\n        // error\n        "+self.errorHandlerName+'("Message '+structt['name']+' to short");\n'
        s += '        return '+ self.CONST_ERR_BUFF_SIZE +';\n      }\n'
        s += "      "+ self.makeLineComment("call the (external user) defined function with the unpacked data")
        s += "      "+ self.funcProcessFunPrefix + structt["name"] +"("
        s += self.makeCallFunArgListAll(structt)
        s += ");\n"
        return s

    # TODO: still used??
    # def genProcessMsgFuns(self):
    #     #find the base messages
    #     s = ""
    #     for st in structList:
    #         if "localConst" in st:
    #             lst     = st["localConst"]
    #             bstruct = self.findInConstList(lst,"MSG_BASE")
    #             # if MSG_BASE is declared, a message select and unpack is created
    #             if bstruct :
    #                 s += self.genMsgFactoryFromBuffFun(st)
    #     return s

    def genUnpackAllFromBuffFun(self,baseStructt):
        s  = self.makeComment("""This is the base message parser that should be called with
        the byte array to be translated to a spesific message
        First determine the struct/message type based on MSG_ID and
        MSG_COND and then unpack""")
        # function declaration 
        s += self.makeUnpackAllFunDecl(baseStructt)
        s += self.FUNCTION_BLOCK_START 
        #s += IND1+self.INT_TYPE_NAME +" "+self.BUF_POS_NAME+" = 0;\n"
        # declare and initialize the buffer index variable
        s += IND1+self.makeStatementDoAssignment(self.BUF_POS_NAME,"0",self.INT_TYPE_NAME)
        # if "parentName" in baseStructt:
        #     s += "\nERROR: base or header should not have a parent\n"
        # build the assignment of the data fields from the buffer
        s += self.genUnpackFields(baseStructt,)
        s += NL
        # code body
        firstif = True
        for st in structList:
            ## first check if a structure was derived from this base
            if self.isInheritedFrom(st,baseStructt):
                # find all the fields that should be used as tags/conditions
                s += IND1+self.makeLineComment("if message is '"+st["name"]+"' inherited from "+baseStructt["name"])
                s1 = ""                  
                for retag in st["retags"]:
                    s1 += "("+retag["name"]+" == "+retag["expr"]+")"
                # generate if tag match, parse message code
                if firstif:
                    s += IND1 + self.makeStatementIf(s1)
                    firstif = False
                else:
                    s += IND1 + self.makeStatementElseIf(s1)
                s += self.genUnpackFields(st,indent =IND2)
                s += NL
                #s += IND1 + self.makeStatementElse()
            # TODO still used ??????
            # if "localConst" in st:
            #     lst = st["localConst"]
            #     s1  = ""
            #     s2  = ""
            #     itm = self.findInConstList(lst,"MSG_ID")
            #     if itm :
            #         if 'expr' in itm:
            #             s1 = "(msg_id == "+itm['expr']+")"
            #         elif 'value' in itm:
            #             s1 = "(msg_id == "+itm['value']+")"
            #     itm = self.findInConstList(lst,"MSG_COND")
            #     if itm :
            #         if 'expr' in itm:
            #             s2 = "("+itm['expr']+")"
            #     if len(s1)>1 and len(s2)>1:
            #         s3 = "    if ("+s1+" && "+s2+") {\n"
            #     else:
            #         s3 = "    if "+s1+s2+" {    // "+ st["name"] + "\n"
            #     if len(s1)>1 or len(s2)>1:
            #         s += s3+ self.genProcessMsgDetail(st)
            #         s += "    } else \n"
            #s += self.genPackFun( st) +"\n"
        s += IND1+ "}  else   "
        s += IND1+ "{     " + self.makeLineComment("error unknown message")
        s += IND2+ self.errorHandlerName+'("Unknown message tag");' + NL
        s += IND2+ 'return '
        s += self.CONST_ERR_UNKNOWN_TAG +';\n    }\n'
        s += IND1+ "return "+self.BUF_POS_NAME+";\n"

        s += self.FUNCTION_BLOCK_END 
        return s

    # TODO: still needed ???
    def genMsgFactoryFromBuffFun(self,baseStructt):
        s  = self.makeLineComment("This is the base message parser that should be called with")
        s += self.makeLineComment("the byte array to be translated to a spesific message")
        s += self.makeLineComment("First determine the struct/message type based on MSG_ID and")
        s += self.makeLineComment("MSG_COND and then unpack")
        # function declatation 
        s += self.genMsgFactoryFromBuffFunDecl(baseStructt,retType="int") 

        s += self.FUNCTION_BLOCK_START
        s += "    "+self.INT_TYPE_NAME +" "+self.BUF_POS_NAME+" = 0;\n"
        if "parentName" in baseStructt:
            s += "\nERROR: base or header should not have a parent\n"
        # build the assignment of the data fields to the buffer
        s += self.genUnpackFields(baseStructt)

        s += "\n"
        # code body
        for st in structList:
            ## first check if a structure was derived from this base
            if self.isInheritedFrom(st,baseStructt):
                s += NL + self.COMMENT_LINE +"generate new struct "+st["name"]+" inherited from "+baseStructt["name"]+"\n"
                s += "if ("                  
                for retag in st["retags"]:
                    s += "("+retag["name"]+" == "+retag["expr"]+")"
                s += ") { \n"                  
                s += "} \n"                  

            if "localConst" in st:
                lst = st["localConst"]
                s1  = ""
                s2  = ""
                itm = self.findInConstList(lst,"MSG_ID")
                if itm :
                    if 'expr' in itm:
                        s1 = "(msg_id == "+itm['expr']+")"
                    elif 'value' in itm:
                        s1 = "(msg_id == "+itm['value']+")"
                itm = self.findInConstList(lst,"MSG_COND")
                if itm :
                    if 'expr' in itm:
                        s2 = "("+itm['expr']+")"
                if len(s1)>1 and len(s2)>1:
                    s3 = "    if ("+s1+" && "+s2+") {\n"
                else:
                    s3 = "    if "+s1+s2+" {    // "+ st["name"] + "\n"
                if len(s1)>1 or len(s2)>1:
                    s += s3+ self.genProcessMsgDetail(st)
                    s += "    } else \n"
            #s += self.genPackFun( st) +"\n"
        s += "    {\n      // error\n      "
        s += self.errorHandlerName+'("Unknown message tag");\n      return '
        s += self.CONST_ERR_UNKNOWN_TAG +';\n    }\n'
        s += "    return "+self.BUF_POS_NAME+";\n"
        s += self.FUNCTION_BLOCK_END
        return s

    def makeComment(self,comment):
        return self.COMMENT_START  + comment + self.COMMENT_END+"\n"
    def makeLineComment(self,comment):
        return self.COMMENT_LINE  + comment +"\n"
    def makeLineCommentDivider(self):
        return self.makeLineComment("---------------------------------------")
    def makeLineCommentDivider2(self):
        return self.makeLineComment("=======================================")
    def makeConstDecl(self,vname,vval):
        return "#define " + vname.ljust(15) + " " +vval+ "\n"
    def genAuxDef(self):
        s  = self.makeLineComment("Constant return values defined")
        s += self.makeConstDecl(self.CONST_ERR_OUT_OF_BOUNDS,"-2")
        s += self.makeConstDecl(self.CONST_ERR_BUFF_SIZE,"-4")
        s += self.makeConstDecl(self.CONST_ERR_UNKNOWN_TAG,"-6")
        s += self.makeConstDecl(self.CONST_ERR_CRC,"-23")
        s += self.makeConstDecl(self.CONST_ERR_NOT_EQ,"-41")
        return s + "\n"

    def genStructFieldDecls(self,structt):
        # for each field, add a declaration
        s  = ""
        for f in structt["body"]:
            if 'value' in f:
              vv = f["value"]
            else:
              vv = None
            s += "  "+self.genVarDecl(f, termstr = ";\n",setInitValue = False)
            #s += "  "+self.genVarDecl(f["type"],f["name"],vv, termstr = ";\n")
        return s
    def genStructDeclBegin(self,structname,parentstruct= None):
        s  = "typedef struct "+structname+" {\n"
        if parentstruct is not None:
            s += "  " + parentstruct['name'] + " ??;\n"
        #s += "{\n \n"
        return s
    def genStructDeclEnd(self,structname):
        s = "} "+structname+ self.TYPE_POSTFIX + ";\n\n"
        return s
    def isStructDeclUsed(self,structt):
        return self.isAnnoTagDefined(structt,'STRUCT')
    #TODO: remove?
    # def genClasses(self):
    #     s  = ""
    #     for st in structList:
    #       #if self.isStructDeclUsed(st):
    #         s += self.genStructDeclBegin(st['name'])
    #         s += self.genStructFieldDecls(st)
    #         s += self.genStructDeclEnd(st['name'])
    #     return s
    def genCstructIfUsed(self,st):
        s  = "\n"
        if self.isStructDeclUsed(st):
            s += self.genStructDeclBegin(st['name'])
            s += self.genStructFieldDecls(st)
            s += self.genStructDeclEnd(st['name'])
        return s


    def genStructPrototypes(self,msgIdArg = False):
        s  = ""
        for structt in structList:
            # normally C struct are not declared but there are circumstances where it is needed
            s += self.genCstructIfUsed(structt)
            # TODO: cleanup this?
            nn = "" #convertCamelToSnake(structt["name"])
            # if self.isStructDeclUsed(structt):
            #     s += self.genUnpackFunDecl(structt,classLike=True)
            # else:
            s += self.genUnpackFunDecl(structt,namePrefix = nn ) #,msgIdArg = msgIdArg,namePrefix = namePrefix)
            s += ";\n"
            # determine if the function should be defined
            ret = self.getLocalAnno(structt,"pack","TRUE")
            if ret == "FALSE" : return ""    # function should not be generated
            # determine if a function call in pack is defined
            callFuncName          = self.getLocalAnno(structt,"call_in_pack","FALSE")
            localBuffAndUserFunct = (callFuncName != "FALSE")
            #parentArgs            = ""

            #s += self.genPackFunDecl(structt,namePrefix = nn)
            s += self.genPackFunDecl(structt,copyToBuff= not localBuffAndUserFunct,msgIdArg = msgIdArg,namePrefix = nn)
            s += ";\n"
            # special additonal struct copy function
            if self.isStructDeclUsed(structt):
                s += "static inline "+self.makePackInStructDecl(structt,inctypedecl=True)
                s += "// only for same indianness\n"
                #s += self.INT_TYPE_NAME + " " +structt["name"]+ "_copy(struct "+structt["name"]+ "*ptr,uint8_t buff[],int "+self.BUF_POS_NAME+")\n"
                siz,res = self.genPackLenCalc(structt)
                siz,res = self.simplifyConstsInEq(siz)
                #s += "{ memcpy(buff,this,"+siz+"); return "+siz+"; }\n"
                s += "{\n    int "+self.BUF_POS_NAME+" = 0;\n"
                s += self.genPackFieldAssignments(structt,structPrefix = "this->")
                s += "    return "+siz+"; \n}\n"

            # in most cases there is one structure that defines the header
            # this is a special struct that needs more code to full fill its role
            # as base from which the other messages are 'created'
            if self.isAnnoTagDefined(structt,"MSG_BASE"):
                s += "\n//============== base =================\n\n"
        return s
    # def makeUserAferUnpackFunDecls(self)
    #     s = ""
    #     for structt in structList:
    #         s += self.makeUserAferUnpackFunDecl(structt)
    #     return s

    def genUserFuncDecls(self):
        s  = ""
        for structt in structList:
            # normally C struct are not declared but there are circumstances where it is needed
            #s += self.genCstructIfUsed(structt)
            funname   = self.makeFunNameUserAferUnpack(structt)
            if funname is not None:
                s +=  self.LEN_TYPE_NAME + " "
                s +=  funname
                s +=  "("  
                s += self.makeDeclUserAferUnpackFunArgList(structt)
                s +=  ");" + NL 
            # TODO ?? still needed
            s += self.makeUserAferUnpackFunDecl(structt, True)
            if self.isStructDeclUsed(structt):
                s += self.genUnpackFunDecl(structt,copyToBuff=True,classLike=False,namePrefix = "") + ";\n"

                #args = structt["name"]+self.TYPE_POSTFIX +"* this,"+args
                #s += self.makeFuncDeclr(structt,self.INT_TYPE_NAME,namePrefix+self.funcUnpackBaseName,args)
            s += self.makeUserProcessMsgDecl(structt)
        return s

    def genAll(self,hfilename,cfilename= None,userfile=None):
        #pp = hfilename.rfind(".h")
        basename = hfilename.replace(".h","")
        #if pp > 1: basename = basename[:pp]
        if cfilename == None: cfilename = basename + ".c"
        if userfile == None: userfile = basename + "User.h"
        # generate the prototypes for the functions that must be defined/suplied by  the user
        su  = ""
        su += self.genFileHeader(userfile)
        su += self.makeLineComment("User implemented function declarations")
        su += self.makeLineComment("These functions are called by the generated code")
        su += self.makeLineComment("but must be implemented by the user")
        su += self.makeLineComment("Note that some might not be needed")
        hprot = "__"+userfile.upper().replace(".","_").replace("/","_") +"__"
        su += "#ifndef "+hprot +"\n"
        su += "#define "+hprot +"\n" + NL
        su += '#include "'+hfilename +'"\n'
        su += '#include "inttypes.h"' + NL + NL + NL
        su += self.genUserFuncDecls()
        su += "#endif  // "+hprot +"\n"
        # generate header
        sh  = ""
        sh += self.genFileHeader(hfilename)
        hprot = "__"+hfilename.upper().replace(".","_").replace("/","_") +"__"
        sh += "#ifndef "+hprot +"\n"
        sh += "#define "+hprot +"\n"
        sh += annotateDict.get('c_includes',"")
        sh += '#include "inttypes.h"' + NL + NL + NL
        sh += self.genAuxDef()
        sh += self.makeLineCommentDivider()
        sh += self.genAllEnumDefs()
        # TODO: remove???? - now done as part of structure declarations
        #sh += self.genCstructs()
        sh += self.makeLineCommentDivider()
        sh += self.makeLineComment("Function declarations")
        sh += self.makeLineCommentDivider()
        sh += self.genStructPrototypes()
        sh += "#endif  // "+hprot +"\n"

        sc  = "// C code \n"
        sc += self.genFileHeader(cfilename)
        sc += '#include "'+hfilename +'"\n'
        sc += '#include "'+userfile +'"\n'
        sc += '#include "string.h"\n'
        sc += annotateDict.get('c_includes',"")
        sc += annotateDict.get('c_code',"")
        for structt in structList:
            if self.isAnnoTagDefined(structt,"MSG_BASE"):
              ret = self.getLocalAnno(structt,"factory","TRUE")
              if ret != "FALSE" :     # function should not be generated
                sc += NL+ self.makeLineComment("=== BASE parse and unpack byte array =====================" + NL)
                sc += self.genUnpackAllFromBuffFun(structt)
            sc += self.genPackFun( structt) +NL
            #sc += self.genUnpackFun( structt) +"\n"
        #sc += self.genProcessMsgFuns()
        return sh,sc,su



# generate python code - non object orientated
class PyCodeGenerator(CcodeGenerator):
    privateFuncDecl      = NL+"    # private should only be used locally"
    singletonFuncDecl    = "@classmethod" + NL
    FUNCTION_BLOCK_START = ""
    FUNCTION_BLOCK_END   = "# end\n"
    STATEMENT_END        = ""
    COMMENT_START        = '"""'
    COMMENT_END          = '"""'
    COMMENT_LINE         = "# "
    #BUF_TYPE_NAME        = 'buff'
    BUF_FUN_ARG          = CcodeGenerator.BUF_NAME +" "
    BUF_SIZE_ARG         = CcodeGenerator.BUF_SIZE_NAME + " "
    BUF_LEN_ARG          = CcodeGenerator.BUF_LEN_NAME
    typeTable2          = {"uint8":"int","string":"pystring","zstring":"char","ustring":"wchar","uzstring":"wchar"}



    def BUF_DECLARE(self,len=512):  return  self.BUF_NAME+ " bytearray("+str(len)+")"

    def lookupType(self,vartype):
        return ""
        #if vartype in self.typeTable2: return  self.typeTable2[vartype]
        #if vartype in self.typeTable1: return  self.typeTable1[vartype]
        #return vartype+self.TYPE_POSTFIX
    # def lookupFieldType(self,fieldd):
    #     vartype = fieldd["type"]
    #     if vartype.startswith("enum"):
    #         return "enum "+fieldd["enumName"]
    #     if vartype in self.typeTable2: return  self.typeTable2[vartype]
    #     if vartype in self.typeTable1: return  self.typeTable1[vartype]
    #     return vartype+self.TYPE_POSTFIX
    def makeFuncDeclr(self,structt,retType,funName,args,isSingletonFun = False,isPrivate = False):
        """ Make a valid function declaration

        Arg:
          funName    - the base name (root) of the name. If it is part of a class
                       it should be prefixed wih the class name (or what ever the
                       programming language require)
          args       - function arguments
          isSingletonFun  specifies whether it is a static class member
          isPrivate       means that it will not be called externally
        """
        s = ""
        if isPrivate:      s+= self.privateFuncDecl
        if isSingletonFun: s+= self.singletonFuncDecl
        #nn = structt['name'].upper() + "_" + funName
        nn = convertCamelToSnake(structt['name']) + "_" + funName
        s += "def " + nn + "(" + args + "):" + NL
        #s += retType + " " + nn + "(" + args + ")"
        return s
    def makeStatementIf(self,condition):
        return "if "+condition+":" + NL
    def makeStatementElse(self):
        return "else: " + NL
    def makeStatementElseIf(self,condition):
        return "elif ("+condition+"):" + NL
    def makeStatementEndIf(self):
        return "# end if"
    def makeStatementStartCase(self,varName):
        return "??switch ("+varname+") {" + NL
    def makeStatementCase(self,constVal):
        return "??case "+constVal+":" + NL
    def makeStatementEndCase(self):
        return "??} // switch" + NL
    # variable declarations
    def makeStatementDeclareVar(self,varName,varType,assignedVal=""):
        if assignedVal != "": return varName + " = "+ assignedVal +"     # type is "+varType + NL
        return "# var: " +varName + " = "+ varType +"("+ assignedVal +")" + NL    
    # assignments
    def makeStatementDoAssignment(self,varName,assignedVal,varType = ""):
        #if varType != "": return varName + " = "+ assignedVal +"     # type is "+varType + NL
        #return varName + " = "+ varType +"("+ assignedVal +")" + NL
        return varName + " = "+  assignedVal  + NL
    def makeStatementIncrement(self,varName,incVal):
        return varName + " += "+ incVal +"" + NL
    def makeCondError(self,condi,msg=""):
        return "if "+condi+": return ret\n"
    def makeAssignByteToArr(self,byteArrName,arrIndx,byteVal):
        return byteArrName + "[" + arrIndx +"] = "+byteVal+"\n"

    def __init__(self):
        #self.typeTable1           = {"bool":"bool","enum8":"uint8_t","char":"char","string":"String"}
        self.typeTable1           = {"bool":"","enum8":"","char":"","string":""}
        #typeTable2           = {"string":"","zstring":"char","ustring":"wchar","uzstring":"wchar"}
        self.codeCommentStart    = "''' "
        self.codeCommentEnd      = " '''"
        self.codeCommentLineStart= "# "
        self.doxPre              = "\n    :"         # pydoc prefix
        self.doxPost             = ":"         # pydoc prefix
        # set the language
        self.LANG = BaseCodeGenerator.LANG_PY
        #self.singletonFuncDecl   = NL + IND1 + "@classmethod" + NL


    def genAll(self,filename,userfile=None):
        if userfile == None: userfile = filename + "User.py"
        # generate the prototypes for the functions that must be defined/suplied by  the user
        su  = ""
        #su += self.genFileHeader(userfile)
        su += self.makeLineComment("User implemented function declarations")
        su += self.makeLineComment("These functions are called by the generated code")
        su += self.makeLineComment("but must be implemented by the user")
        su += self.makeLineComment("Note that some might not be needed")
        #hprot = "__"+userfile.upper().replace(".","_").replace("/","_") +"__"
        su += 'import '+filename + NL
        su += self.genUserFuncDecls()

        sc  = "# pyhon code \n"
        sc += annotateDict.get('py_packages',"")
        sc += annotateDict.get('py_code',"")
        for structt in structList:
            if self.isAnnoTagDefined(structt,"MSG_BASE"):
              ret = self.getLocalAnno(structt,"factory","TRUE")
              if ret != "FALSE" :     # function should not be generated
                sc += NL+ self.makeLineComment("=== BASE parse and unpack byte array =====================" + NL)
                sc += self.genUnpackAllFromBuffFun(structt)
            sc += self.genPackFun( structt) +NL
            #sc += self.genUnpackFun( structt) +"\n"
        #sc += self.genProcessMsgFuns()
        return sc,su


    


# class oldOOcodeGenerator(CcodeGenerator):
#     typeTable1      = {"bool":"bool","enum8":"uint8_t","char":"char","string":"String"}
#     def __init__(self):
#         # set the language
#         self.LANG = BaseCodeGenerator.LANG_CPP
#     def genClassDeclBegin(self,structname,parentstruct= None):
#         s = "class "+structname+" "
#         if parentstruct is not None:
#             s += ": public " + parentstruct + "\n"
#         s += "{\n public:\n"
#         return s
#     def genClassDeclEnd(self,structname):
#         s = "}; // end class "+structname+ "\n\n"
#         return s

#     def genClassDef(self,struct):
#         parent = struct.get("parentName",None)
#         s  = self.genClassDeclBegin(struct["name"],parent)
#         #  for each field, add a declaration
#         s += self.genStructFieldDecls(struct)
#         # for f in struct["body"]:
#         #     if 'value' in f:
#         #       vv = f["value"]
#         #     else:
#         #       vv = None
#         #     s += "  "+self.genVarDecl(f, termstr = ";\n")
#         #     #s += "  "+self.genVarDecl(f["type"],f["name"],vv, termstr = ";\n")
#         # generate the serialze and deserialize functions for the struct
#         s1 = self.genCreateFunDecl(struct)
#         s += "  "+addIndent(s1) + ";\n"
#         s1 = self.genMsgFactoryFromBuffFunDecl(struct)
#         s += "  "+addIndent(s1) + ";\n"
#         s1 = self.genPackFunDecl(struct)
#         s += "  "+addIndent(s1) + ";\n"
#         s1 = self.genUnpackFunDecl(struct)
#         s += "  "+addIndent(s1) + ";\n"
#         s += self.genClassDeclEnd(struct["name"])
#         return s
#     def genClassImplementation(self,struct):
#         parent = struct.get("parentName",None)
#         s  = self.makeLineCommentDivider2()
#         s += self.makeLineComment( "Class "+struct["name"] + " implementation")
#         s += self.makeLineCommentDivider()
#         # generate the serialze and deserialize functions for the struct
#         s += self.genPackFun(struct,namePrefix = struct['name']+"::")
#         s += self.genUnpackFun(struct,namePrefix = struct['name']+"::",callParentUnpack=True)
#         #s += "  "+addIndent(s1) + "\n"
#         return s
#     def genAll(self,hFileName,cFileName):
#         # generate h file -------
#         s  = self.genFileHeader(hFileName)
#         s += "\n"
#         s += '#include "inttypes.h"\n'
#         #s += annotateDict.get('copyrigh',"")
#         s += annotateDict.get('h_includes',"")
#         s += self.genAllEnumDefs()
#         s += self.genAuxDef()
#         for st in structList:
#             s += self.genClassDef( st) +"\n"

#         # generate cpp file -------
#         ss  = self.genFileHeader(cFileName)
#         ss += "\n"
#         ss += '#include "' + hFileName + "\n"
#         ss += annotateDict.get('c_includes',"")
#         ss += annotateDict.get('c_code',"")
#         for st in structList:
#             ss += self.genClassImplementation( st) +"\n"
#         return s,ss



# generate documentation
class MarkdownGenerator(CcodeGenerator):
    H1     = "\n# "
    H2     = "\n## "
    H3     = "\n### "
    LIST   = "* "
    BOLT   = "**"
    LINE   = "___\n"
    DLINE  = "___\n___\n"
    NLNL   = "\n"
    BR     = "\n\n"
    COL    = "|"
    ROW    = "\n|"
    def genInternalLink(m,ltext,linktag=None):
        if linktag is None: linktag= ltext
        #if ltext in enumList:
        #   s ="["+ltext+"](#"+"enum-" + ltext.lower() +")"
        #elif ltext in structList:
        s ="["+ltext+"](#" + linktag.lower() +")"
        return s
    # def checkForLink(m,ltext):
    #     # search through the ext o see if there are any
    #     #if ltext in enumList:
    #     #   s ="["+ltext+"](#"+"enum-" + ltext.lower() +")"
    #     #elif ltext in structList:
    #     s ="["+ltext+"](#" + linktag.lower() +")"
    #     return s
    def genDocEnums(m):
        s  = m.H2 + "Enumerations\n" + m.LINE
        for enm in enumList:
            s += m.H3 + "Enum " + enm["enumName"] +"\n"
            if "comment" in enm:
                s += cleanstr(enm["comment"]) +m.NLNL
            s += m.ROW + "Tag" + m.COL + "Value" + m.COL + "Comment"  + m.COL
            s += m.ROW + "------" + m.COL + "-----" + m.COL + "------------------------------"  + m.COL
            for vv in enm["values"]:
                if "comment" in vv: cmt = cleanstr(vv["comment"])
                else: cmt = ""
                if "comment2" in vv: cmt += " " +vv["comment2"]
                s += m.ROW + vv["name"] + m.COL + vv["value"] + m.COL + cleanstrNL(cmt) + m.COL
            s += m.NLNL
        return s
    def makeEnumLinkIfContains(m,symb):    
        enumm,fmemb = m.findEnumMember(symb)
        if enumm != None: return m.genInternalLink(symb,"enum-"+enumm["enumName"])
        return symb    

    def genDocStructs(m):
        s  = m.H2 + "Structures\n" + m.LINE
        for st in structList:
            #s += m.LINE
            s += m.H3 + st["name"] +"\n"
            if "comment" in st:
                s += cleanstr(st["comment"]) +m.NLNL
            if "retags" in st:
                s += m.BR + m.BOLT+ "Message tag values" + m.BOLT+ m.BR
                s += "The values of the following field(s) from parent classes are overridden" + m.NLNL
                s += m.ROW + "Assign to field" + m.COL + "Value" + m.COL + "From" + m.COL + "Comment"  + m.COL
                s += m.ROW + "------" + m.COL + "-----" + m.COL +"-----" + m.COL + "------------------------------"  + m.COL
                for tag in st["retags"]:
                    parnt = "-"
                    if "parentName" in st: parnt = st["parentName"]
                    if "comment2" in tag: cmt = cleanstr(tag["comment2"])
                    else: cmt = ""
                    # find the message/struct where the field is declared
                    (ss,ff) = m.findFieldName(st,tag["name"])
                    if ss != None:
                        parnt = m.genInternalLink(ss["name"])
                    else:  parnt = "Error not defined"
                    valstr = m.makeEnumLinkIfContains(tag["expr"])    
                    s += m.ROW +  tag["name"]  + m.COL + valstr + m.COL +  parnt+m.COL +cmt + m.COL
                # s += m.BR + m.BOLT+ "Message TAGS" + m.BOLT+ m.NLNL
                # for tag in st["retags"]:
                #     s += m.LIST +  tag["name"]  + "  =  " + tag["expr"] + m.NLNL
            if "anno" in st:
                s += m.BR + m.BOLT+ "Annotations" + m.BOLT+ m.NLNL
                for loc in st["anno"]:
                    #s1 = loc.get("value","") + loc.get("eval","") + loc.get("sval","")
                    s1 = ""
                    s += m.LIST +  loc["name"]  + "  =  " + s1 + m.NLNL
            # TODO: not sure this should be kept - replaced with RETAGS and global annotations/enums
            if "localConst" in st:
                s += m.BR + m.BOLT+ "Structure locals constants" + m.BOLT+ m.NLNL
                for loc in st["localConst"]:
                    s1 = loc.get("value","") + loc.get("eval","") + loc.get("expr","")
                    s += m.LIST +  loc["name"]  + "  =  " + s1 + m.NLNL
                #if table
                # s += "Structure value definitions " + m.NLNL
                # s += m.ROW + "Symbol" + m.COL + "Value" + m.COL + "Comment"  + m.COL
                # s += m.ROW + "------" + m.COL +"-----" + m.COL + "------------------------------"  + m.COL
                # for loc in st["localConst"]:
                #     s1 = loc.get("value","") + loc.get("eval","") + loc.get("sval","")
                #     s += m.ROW +  loc["name"]  + m.COL + s1 + m.COL + " " + m.COL
                # s += m.NLNL

            if "parentName" in st:
                s += m.BR + "Structure inherits fields from " + m.BOLT
                s += st["parentName"] + m.BOLT + " and add these" + m.BR
                parentstr = m.ROW + "Parent" + m.COL + m.genInternalLink(st["parentName"]) + m.COL + "" + m.COL + "This data prepend this structure"  + m.COL
                #parentstr = m.ROW + "Parent" + m.COL + st["parentName"] + m.COL + "" + m.COL + "This data prepend this structure"  + m.COL
            else:
                parentstr = ""
                s += "Fields in this structure" + m.BR
            s += m.ROW + "Field" + m.COL + "Type" + m.COL + "Array" + m.COL + "Comment"  + m.COL
            s += m.ROW + "------" + m.COL + "-----" + m.COL +"-----" + m.COL + "------------------------------"  + m.COL
            s += parentstr
            for f in st["body"]:
                if "comment" in f: cmt = cleanstr(f["comment"])
                else: cmt = ""
                if "comment2" in f: cmt += " " +f["comment2"]
                if "arrLen" in f:
                    arrLen = f["arrLen"]
                else:
                    arrLen = "-"
                # if "retagid" in f:
                #     continue
                #     #etxt  = f["retagid"]
                #     vtype = f[""] + " " + m.genInternalLink(etxt,"enum-"+etxt)
                # el
                if f["type"].find("enum")>=0:
                    etxt  = f["enumName"]
                    vtype = f["type"] + " " + m.genInternalLink(etxt,"enum-"+etxt)
                elif f["type"] in structDict:
                    etxt  = f["type"]
                    vtype = f["type"] + " " + m.genInternalLink(etxt,etxt)
                else: vtype = f["type"]
                vname = f["name"]
                if "value" in f: vname += " = " + f["value"]    
                if "annoMsgTag" in f: vname = "[" + vname + "] is a message tag "
                s += m.ROW + vname + m.COL + vtype + m.COL + arrLen + m.COL + cleanstrNL(cmt) + m.COL
            slen,ret = m.genPackLenCalc(st)
            simlen,canSimplify = m.simplifyConstsInEq(slen)
            if canSimplify : simlen = simlen
            else : simlen = "variable"
            s += m.ROW + "Total" + m.COL + " length" + m.COL + simlen + m.COL + slen  + m.COL
            s += m.NLNL
        return s

    def genDocHeader(m):
        s = ""
        if "doc_header" in annotateDict:
            s += m.H1 +  cleanstr(annotateDict["doc_header"])+ m.NLNL
        else:
            s += m.H1 +  "Title" + m.NLNL
        s += m.H3
        if "name" in annotateDict:
            s += cleanstr(annotateDict["name"])
        if "version" in annotateDict:
            s += " version " + cleanstr(annotateDict["version"])
        s +=  m.NLNL
        s += m.LINE
        if "doc_intro" in annotateDict:
            s += m.H2 + "Intoduction\n" + cleanstr(annotateDict["doc_intro"]) + m.NLNL
        return s
    def genAll(m):
        s  = m.genDocHeader()
        s += m.DLINE
        s += m.genDocEnums()
        s += m.DLINE
        s += m.genDocStructs()
        return s




# class oldOOpythonGenerator(oldOOcodeGenerator):
#     def __init__(self):
#         self.typeTable1           = {"bool":"bool","enum8":"uint8_t","char":"char","string":"String"}
#         self.COMMENT_START    = "''' "
#         self.COMMENT_END      = " '''"
#         self.COMMENT_LINE = "# "
#         self.doxPre              = "\n    :"         # pydoc prefix
#         self.doxPost             = ":"         # pydoc prefix
#         #typeTable2           = {"string":"","zstring":"char","ustring":"wchar","uzstring":"wchar"}
#         # set the language
#         self.LANG = BaseCodeGenerator.LANG_PY
#     def lookupType(self,vartype):
#         return ""
#     def genEnumStart(self,enm):
#         return  "\nclass "+enm["enumName"] + "(IntEnum):"
#     def genEnumEnd(self,enm):
#         return  "\n"
#     def genClassDeclBegin(self,structname,parentstruct= None):
#         s = "class "+structname
#         if parentstruct is not None:
#             s += "(" + parentstruct + ")"
#         return s + ":\n"
#     def genClassDeclEnd(self,structname):
#         return "\n\n"
#     def genAll(self,fname):
#         s  = "from enum import Enum\n"
#         s += annotateDict.get('py_imports',"")
#         s += annotateDict.get('py_code',"")
#         s += self.genAllEnumDefs(separator = "")
#         for st in structList:
#             s += self.genClassImplementation( st) +"\n"

#         return s





# test1 = """
# @name       =  "Test BBX"
# @version    =  "0.1-4"
# @doc_title  =  "BBX document"
# @doc_header =  "BBX document heading generation Testing"
# @doc_intro  =  '''This is the definiton of the message protocol
#                   used for bla-bla-bla'''

# @c_includes =  '''
# #include <stdio.h>
# #include "comms.h"
# '''

# @c_code = '''
# void testfun(void)
# {
#    dosomething();
# } // end test
# '''

# /*
#  This is the common header for everybody.
# */
# struct GGheader
# {
#     /* This is to test a very long comment line.
#        The destination is where the message should be send to.
#        Each device should be allocated a fixed address.
#     */
#   uint8     dest;   // Alt dest comment
#   MSGID16   mM; // This is to test a very long comment line. Abcdefghijklmnopqrstuvwxyz abcdefghijklmnopqrstuvwxyz
#   uint16    msg_id = MSG_ID; // This is to test a very long comment line. Abcdefghijklmnopqrstuvwxyz abcdefghijklmnopqrstuvwxyz
#   CRC16     CalcCrc16[dest:msgid];  //  CRC16     CalcCrc16(dest..msgid);
# }

# enum Gender {
#     UNKNOWN = 0;
#     MALE    = 1;   // set as male
#     FEMLE   = 0x2;   // set as female
#     OTHER   = 0x3;   // if a person identifies with a different gender
# }

# /*
#   This is the message to set the usmakeDeclUserAferUnpackFunArgList
#   Line 2 of comment
# */
# struct SetProfile headedby GGheadermakeDeclUserAferUnpackFunArgList
# @CC=56
# @CV=542
# <MSG_ID = 0x1155>  <ARRLEN = 10>
# {
#   int32 id = 1;      // the user identificatin number
#   char[20]  surname; // the user surname
#   enum8     ename  fieldvarname {  x1=1; x2=2; a1 = 3;  };

#   enum8  Gender gender;
#   int8     dlen;
#   CRC16     hdrCrc2[dest:dlen]; // a message calculated crc
#   char[dlen]    addit;
#   zstring email = "eeeeee";
# }"""




# GGcomsDef = """
# @name       =  "GGCommsDefinition"
# @version    =  "1.0-0"
# @doc_title  =  "Greatguide Communications Protocol Definition"
# @doc_header =  "BXB definition document"
# @doc_intro  =  '''This is the definiton of the message protocol
#                   used between the seat units and the master streamer'''

# @c_includes =  '''
# #include <stdio.h>
# #include "comms.h"
# '''

# @c_code = '''
# void testfun(void)
# {
#    dosomething();
# } // end test
# '''

# /* These are the main commands.
# */
# enum cmd_t {
#     CMD_BROADCAST      = 0x0A;  // All messages using this tag will be broadcasted to all addresses, e.g. audio files
#     CMD_BROADCAST_ACK  = 0x03;  // An ackowladge if the broadcast command was sucessfull
#     CMD_READ           = 0x15;
#     CMD_READ_ACK       = 0x16;
#     CMD_WRITE          = 0x29;
#     CMD_WRITE_ACK      = 0x30;  // Acknowladge send on a write
#     CMD_WR_SLAVE       = 0x3D;  // Not used: Reserved to be used to write to a single seat unit.
#     CMD_DEBUG          = 0x1A;  // Debug command only and should be ignored otherwise.
#     CMD_NACK           = 0x04;  // A negative ackowladge used if any command failed
#     CMD_ACK_HEADER     = 0x05;  // An ackowladge used if the header was recieved withhout error.
#     }


# /*
#  This is the common header for all messages.
# */
# struct MsgHeader
# {
#     /* This is a magic number that indicates the start of the message */
#     uint32  magic = 0x900DBEEF;
#     /*
#        The destination address is where the message should be send to.
#        Devices are allocated a fixed address. The address for the master streamer
#        is always 0. Seat units are each configured with ther own addresses.
#        Messages can be either directed to a single device or broadcasted
#        depending on the command. If it is a broadcast command the destination
#        will still be for a spesific address and that address should send CMD_BROADCAST_ACK
#        acknowladges or a CMD_ACK_HEADER on sucess or a CMD_NACK on an error.
#        When broadcasted the broadcast address
#     */
#     uint8       destAddr;
#     uint8       sourceAddr;         // The source address.
#     enum8 cmd_t    cmd    = CMD_ID; // This is the message identifier.

# }

# /*
#   The read command reads information from the seat units, normally statistics
#   and state changes. The seat unit should respond with a CMD_READ_ACK or
#   CMD_NAC on failure.
# */
# struct ReadMsg headedby MsgHeader
# @CC=56
# @CV=542
# <CMD_ID = 0x15>  <ARRLEN = 10>
# {

#     enum8 read_t   subCmd ;         //
#     uint16         len    = 0;     // no data send with this message
#     /*
#       A sequence number assosiated with this message and returned
#       by the CMD_READ_ACK
#     */
#     uint16         seqNr;       // crc use for integrity checking
#     CRC16          crc16[destAddr:seqNr];
# }

# enum Gender {
#     UNKNOWN = 0;
#     MALE    = 1;   // set as male
#     FEMLE   = 0x2;   // set as female
#     OTHER   = 0x3;   // if a person identifies with a different gender
# }

# /*
#   This is the message to set the user profile.
#   Line 2 of comment
# */
# struct SetProfile headedby MsgHeader
# @CC=56
# @CV=542
# <MSG_ID = 0x1155>  <ARRLEN = 10>
# {
#   int32 id = 1;      // the user identificatin number
#   char[20]  surname; // the user surname
#   enum8     ename  fieldvarname {  x1=1; x2=2; a1 = 3;  };

#   enum8  Gender gender;
#   int8     dlen;
#   CRC16     hdrCrc2[dest:dlen]; // a message calculated crc
#   char[dlen]    addit;
#   zstring email = "eeeeee";
# }"""

# def mainTest2():
#     parseBxbDefStr(GGcomsDef)
#     docgen = MarkdownGenerator()
#     s = docgen.genAll()
#     print(s)


# def mainTest():
#     pp = parser.parseString(test1)
#     pygen = OOpythonGenerator()
#     pygen.pprint()
#     s = pygen.genAll()
#     print(s)
#     print("\n------------------------------\n\n")
#     oogen = OOcodeGenerator()
#     #oogen.pprint()
#     s = oogen.genAll("test.hpp","test.cpp")
#     print(s)

# if __name__ == "__main__":
#     # execute only if run as a script
#     mainTest2()