from genBase import *

# globals for indents
# simply change these if you want to change the indent size
def indnt(spcs):
    if spcs <= 0: return ""
    return indnt(spcs-1) + "    "

IND1 = indnt(1)
IND2 = indnt(2)
IND3 = indnt(3)
NL   = '\n'



class OOcodeGenerator(CcodeGenerator):
    typeTable1      = {"bool":"bool","enum8":"uint8_t","char":"char","string":"String"}
    staticFunDecl   = "static "                  # will be private if in a class

    def __init__(self):
        # set the language
        self.LANG = BaseCodeGenerator.LANG_CPP
    def isStructDeclUsed(self,structt):
        return False        
#    def makeAssignByteToArr(self,byteArrName,arrIndx,byteVal):
#        return byteArrName + "[" + arrIndx +"] = (uint8_t)(this."+byteVal+");\n"
    def makeFuncDeclr(self,structt,retType,funName,args,isSingletonFun = False,isPrivate = False):
        """ Make a valid function declaration

        Arg:
          funName    - the base name (root) of the name. If it is part of a class
                       it should be prefixed wih the class name (or what ever the 
                       programming language require)
          args       - function arguments
          isSingletonFun  specifies whether it is a static class member 
          isPrivate       means that it will not be called externally
        """
        s = ""
        if isPrivate:      s+= self.privateFuncDecl
        if isSingletonFun: s+= self.singletonFuncDecl
        #nn = structt['name'].upper() + "_" + funName
        #nn = convertCamelToSnake(structt['name']) + "_" + funName
        nn = funName
        s += retType + " " + nn + "(" + args + ")"
        return s

    def genUnpackFun(self,structt,callParentUnpack=True,classLike=False,namePrefix = ""):
        # determine if the function should be defined 
        ret = self.getLocalAnno(structt,"unpack","TRUE")
        if ret == "FALSE" : return ""    # function should not be generated
        s  = self.genUnpackFunDecl(structt,classLike=False,namePrefix = namePrefix)
        s += "\n{ \n"
        s += IND1 + self.BUF_POS_DECLARE+";" + NL
        #s += "    "+self.intTypeName +" "+self.buffPosName+" = 0;\n"
        if "parentName" in structt:
            prn           = structt["parentName"]
            parentStruct  = structDict[prn]
            if callParentUnpack:
                funname = prn+"::"+self.funcUnpackBaseName
                # s += "    "+self.buffPosName+" += "+self.makeFuncDeclr(structt,"",funname,args = "buff,buflen-"+self.buffPosName+"") + ";\n"
                s += "    "+self.BUF_POS_NAME+" += "+self.makeFuncDeclr(structt,"",funname,args = "buff,buflen")+";\n"
            else:
                s += self.genUnpackFields(parentStruct)

        # build the assignment of the data fields from the buffer
        s += self.genUnpackFields(structt,stuctderef="")
        # make sure that the buffer had enough data
        s += self.makeStatementIf(self.BUF_POS_NAME+" > " +self.BUF_LEN_NAME)
        s += " return "+ self.CONST_ERR_BUFF_SIZE +";\n"
        #s += "if ( "+self.buffPosName+" > buflen) return "+ self.constErrBuffShort +";\n"

        # call the user function only if not implemented as struct
        #if not self.isStructDeclUsed(structt):
        #    s += self.makeUserAferUnpackFunDecl(structt)
        s += "    return  "+self.BUF_POS_NAME+";\n"
        s += self.makeStatementEndIf()
        #s += "} // end\n"
        return s
    def genPackFun2(self,structt,msgIdArg = False,namePrefix = ""):
        # determine if the function should be defined 
        ret = self.getLocalAnno(structt,"pack","TRUE")
        if ret == "FALSE" : return ""    # function should not be generated
        # determine if a function call in pack is defined
        callFuncName          = self.getLocalAnno(structt,"call_in_pack","FALSE")
        localBuffAndUserFunct = (callFuncName != "FALSE")
        #parentArgs            = ""
        #s  = self.genPackFunDecl(structt,copyToBuff= not localBuffAndUserFunct,msgIdArg = msgIdArg,namePrefix = namePrefix)
        s  = self.genPackFunDecl(structt,copyToBuff= True,msgIdArg = msgIdArg,namePrefix = namePrefix)
        s += "\n{\n"
        #-- generate constant declaraions for the struct local constant assignments
        locConst = structt.get("localConst",[])    
        for itm in locConst:
            valexp = itm.get("value","")+itm.get("expr","")
            s1 = self.makeConsVarDecl(self.intTypeName,itm["name"],valexp)
            s +=  "    "+ (s1) +"\n"
        #-- calculate the length of the data
        (structLen,__) = self.genPackLenCalc(structt)
        s += "    const "+self.intTypeName +" "+self.buffArrName+"Len = " + structLen
        s += ";\n"
        s += "          "+self.intTypeName +" "+self.buffPosName+"    = 0;\n"
        ##if localBuffAndUserFunct:
        ##    s += "    uint8_t   "+self.buffArrName+"["+self.buffArrName+"Len];\n"
        ##else:
        ##    s += "    if ("+self.buffArrName+"Len > bufSize) return "+self.constErrBuffShort+";   "+self.makeLineComment("buffer to small")
        if "parentName" in structt:
            # fill in the inherited data from the parent
            prn           = structt["parentName"]
            parentStruct  = structDict[prn]
            s += self.genPackFields(parentStruct)
        #-- build the assignment of the data fields to the buffer
        s += self.genPackFields(structt)
        # if a funcion must be called before return 
        if localBuffAndUserFunct:
            s += "    return  "+callFuncName+"("+self.buffArrName
            s += ", "+self.buffPosName+");\n"
        else:
            s += "    return  "+self.buffPosName+";\n"
        s += "\n} // end\n"
        return s
    def genPackFun(self,structt,msgIdArg = False,namePrefix = ""):
        # determine if the function should be defined 
        ret = self.getLocalAnno(structt,"pack","TRUE")
        if ret == "FALSE" : return ""    # function should not be generated
        # determine if a function call in pack is defined
        callFuncName          = self.getLocalAnno(structt,"call_in_pack","FALSE")
        localBuffAndUserFunct = (callFuncName != "FALSE")
        #parentArgs            = ""
        s  = self.genPackFunDecl(structt,copyToBuff= not localBuffAndUserFunct,msgIdArg = msgIdArg,namePrefix = namePrefix)
        s += "\n{\n"
        #-- generate constant declaraions for the struct local constant assignments
        locConst = structt.get("localConst",[])    
        for itm in locConst:
            valexp = itm.get("value","")+itm.get("expr","")
            s1 = self.makeConsVarDecl(self.INT_TYPE_NAME,itm["name"],valexp)
            s +=  "    "+ (s1) +"\n"
        #-- calculate the length of the data
        (structLen,__) = self.genPackLenCalc(structt)
        s += IND1 + self.makeConsVarDecl(self.INT_TYPE_NAME ,self.BUF_LEN_NAME,structLen)
        #s += "    const "+self.INT_TYPE_NAME +" "+self.buffArrName+"Len = " + structLen
        s += ";\n"
        s += "          "+self.INT_TYPE_NAME +" "+self.BUF_POS_NAME+"    = 0;\n"
        if localBuffAndUserFunct:
            s += IND1 + self.BUF_DECLARE(self.BUF_SIZE_NAME) + NL
            #s += IND1+self.bufTypeName+"  "+self.buffArrName+"["+self.buffArrName+"Len];\n"
        else:
            s += IND1 + self.makeStatementIf(self.BUF_LEN_NAME + " > "+self.BUF_SIZE_NAME) 
            s += IND1 + " return "+self.CONST_ERR_BUFF_SIZE+";   "+self.makeLineComment("buffer to small")
            #s += IND1+"if ("+self.buffArrName+"Len > bufSize) return "+self.constErrBuffShort+";   "+self.makeLineComment("buffer to small")
        if "parentName" in structt:
            # fill in the inherited data from the parent
            prn           = structt["parentName"]
            parentStruct  = structDict[prn]
            #s += self.genPackFields(parentStruct)
            s += "    // call parent pack\n"
            s += IND1+self.BUF_POS_NAME+" += "+prn+"::"+self.funcPackBaseName+"("
            s += self.makeCallFunArgList(parentStruct) 
            s += ");\n"
            s += "    // over write the values changed by this child\n"
        #-- build the assignment of the data fields to the buffer
        s += self.genPackFields(structt)
        # if a funcion must be called before return 
        if localBuffAndUserFunct:
            s += "    return  "+callFuncName+"("+self.buffArrName
            s += ", "+self.BUF_POS_NAME+");\n"
        else:
            s += "    return  "+self.BUF_POS_NAME+";\n"
        s += "\n} // end\n"
        return s

    def genPackMembersFunDecl(self,structt,namePrefix = ""):
        structnme     = structt["name"]
        fields        = structt["body"]
        parentArgs    = ""
        s  = "\n"
        # generate the comments
        s += self.COMMENT_START
        s += "This function packs the member fields into the buffer array. "
        s += self.doxPre + "param buff[]"+self.doxPost+"    buffer into which data should be packed "
        s += self.doxPre + "param pos"+self.doxPost+"       start position in buffer "
        s += self.doxPre + "return if > 0"+self.doxPost+"   position in array of last extracted data"
        s += self.doxPre + "return if < 0"+self.doxPost+"   error in data stream "
        s += "\n" + self.COMMENT_END +"\n"
        #-- build the argument list for the function
        #args = "uint8_t  "+self.buffArrName+"[],"+self.intTypeName +" bufSize "
        args = self.BUF_FUN_ARG+","+self.BUF_SIZE_DECLARE
        
        #-- build the function call
        s += self.makeFuncDeclr(structt,self.INT_TYPE_NAME,namePrefix+self.funcPackBaseName,args)
        return s

        
    def genClassDeclBegin(self,structname,parentstruct= None):
        s = "class "+structname+" "
        if parentstruct is not None: 
            s += ": public " + parentstruct + "\n"
        s += "{\n public:\n"
        return s
    def genClassDeclEnd(self,structname):
        s = "}; // end class "+structname+ "\n\n"
        return s

    def genClassDef(self,struct):
        parent = struct.get("parentName",None)
        s  = self.genClassDeclBegin(struct["name"],parent) 
        #  for each field, add a declaration
        s += self.genStructFieldDecls(struct)
        # for f in struct["body"]:
        #     if 'value' in f:
        #       vv = f["value"]
        #     else:
        #       vv = None
        #     s += "  "+self.genVarDecl(f, termstr = ";\n")
        #     #s += "  "+self.genVarDecl(f["type"],f["name"],vv, termstr = ";\n")
        # generate the serialze and deserialize functions for the struct
        s1 = self.staticFunDecl + self.genCreateFunDecl(struct)
        s += "  "+addIndent(s1) + ";\n"
        s1 = self.staticFunDecl + self.genMsgFactoryFromBuffFunDecl(struct)
        s += "  "+addIndent(s1) + ";\n"
        # two version of static generated: a staic and non static
        s1 = self.genPackFunDecl(struct)
        if s1 != "":
            # change function to static
            pp = s1.find(self.funcPackBaseName)
            pp2 = s1.find(self.INT_TYPE_NAME,pp-15)
            s1  = s1[:pp2] + self.staticFunDecl +s1[pp2:]
            s += "  "+addIndent(s1) + ";\n"
        s1 = self.genPackMembersFunDecl(struct)
        if s1 != "":
            s += "  "+addIndent(s1) + ";\n"
        # generate unpack
        s1 = self.genUnpackFunDecl(struct)
        s += "  "+addIndent(s1) + ";\n"
        s += self.genClassDeclEnd(struct["name"])
        return s
        
    def genClassImplementation(self,structt):
        parent = structt.get("parentName",None)
        s  = self.makeLineCommentDivider2()
        s += self.makeLineComment( "Class "+structt["name"] + " implementation")
        s += self.makeLineCommentDivider()
        # generate the serialze and deserialize functions for the struct
        s += self.genPackFun(structt,namePrefix = structt['name']+"::")
        s += self.genUnpackFun(structt,namePrefix = structt['name']+"::",callParentUnpack=True)

        if self.isAnnoTagDefined(structt,"MSG_BASE"):
                s += "\n//============== base =================\n\n"
                s += self.genMsgFactoryFromBuffFun(structt)  

        #s += "  "+addIndent(s1) + "\n"
        return s
    ## this is for the languages  using a singele class definiton 
    # and implementation decalaration, e.g. python and java
    def genClassDeclAndImplement(self,struct):
        parent = struct.get("parentName",None)
        s  = self.genClassDeclBegin(struct["name"],parent) 
        #  for each field, add a declaration
        s += self.genStructFieldDecls(struct)
        s += self.genClassConstructor(struct)
        # generate the serialze and deserialize functions for the struct
        s1 = self.staticFunDecl + self.genCreateFunDecl(struct)
        s += "  "+addIndent(s1) + ";\n"
        #s1 = self.staticFunDecl + self.genMsgFactoryFromBuffFun(struct)
        s1 = self.genMsgFactoryFromBuffFun(struct)
        s += "  "+addIndent(s1) + ";\n"
        # two version of static generated: a staic and non static
        s1 = self.genPackFunDecl(struct)
        if s1 != "":
            # change function to static
            pp = s1.find(self.funcPackBaseName)
            pp2 = s1.find(self.INT_TYPE_NAME,pp-15)
            s1  = s1[:pp2] + self.staticFunDecl +s1[pp2:]
            s += "  "+addIndent(s1) + ";\n"
        s1 = self.genPackMembersFunDecl(struct)
        if s1 != "":
            s += "  "+addIndent(s1) + ";\n"
        # generate unpack
        s1 = self.genUnpackFunDecl(struct)
        s += "  "+addIndent(s1) + ";\n"
        s += self.genClassDeclEnd(struct["name"])
        return s


    def genAll(self,hFileName,cFileName):
        # generate h file -------
        s  = self.genFileHeader(hFileName)
        s += "\n"
        s += '#include "inttypes.h"\n'
        #s += annotateDict.get('copyrigh',"")
        s += annotateDict.get('h_includes',"")
        s += self.genAllEnumDefs()
        s += self.genAuxDef()
        for st in structList:
            s += self.genClassDef( st) +"\n"

        # generate cpp file -------
        ss  = self.genFileHeader(cFileName)
        ss += "\n"
        ss += '#include "' + hFileName + "\n"
        ss += annotateDict.get('c_includes',"")
        ss += annotateDict.get('c_code',"")
        for st in structList:
            ss += self.genClassImplementation( st) +"\n"
            scc = self.simplifyAssignment(st,"12*MSG_ID+MLEN==2-crc($BUFF$[445+$$msg_id])-$destAddr")
            print (scc)
            llst = self.findParentFieldsUsingConsts(st)
            print(llst)
        return s,ss



class OOpythonGenerator(OOcodeGenerator):
    privateFuncDecl      = NL+"    # private should only be used locally"
    singletonFuncDecl    = "@classmethod" + NL
    BLOCK_START_FUNCTION = "\n"
    BLOCK_END_FUNCTION   = "# end\n"
    STATEMENT_END        = " "
    COMMENT_START        = '"""'
    COMMENT_END          = '"""'
    COMMENT_LINE         = "# "
    STATEMENT_END        = ""

    def __init__(self):
        self.typeTable1           = {"bool":"bool","enum8":"uint8_t","char":"char","string":"String"}
        self.codeCommentStart    = "''' "
        self.codeCommentEnd      = " '''"
        self.codeCommentLineStart= "# "
        self.doxPre              = "\n    :"         # pydoc prefix
        self.doxPost             = ":"         # pydoc prefix
        #typeTable2           = {"string":"","zstring":"char","ustring":"wchar","uzstring":"wchar"}
        # set the language
        self.LANG = BaseCodeGenerator.LANG_PY
        #self.singletonFuncDecl   = NL + IND1 + "@classmethod" + NL

    def lookupType(self,vartype):
        return ""
    def genStructFieldDecls(self,struct):
        """Generate the declarations in the class for each field.
           In python this is empty
        """
        return ""
    def genEnumStart(self,enm):
        return  "\nclass "+enm["enumName"] + "(IntEnum):"
    def genEnumEnd(self,enm):
        return  "\n"
    # override
    def makeFuncDeclr(self,structt,retType,funName,args,isSingletonFun = False,isPrivate = False):
        """ Make a valid function declaration

        Arg:
          funName    - the base name (root) of the name. If it is part of a class
                       it should be prefixed with the class name (or what ever the
                       programming language require)
          args       - function arguments
          isSingletonFun  specifies whether it is a static class member - make calssfunction in python
          isPrivate       means that it will not be called externally - ignored inpython
        """
        s = ""
        s += self.makeLineComment("function returns a "+retType)
        if isPrivate:      s+= self.privateFuncDecl
        if isSingletonFun: s+= self.singletonFuncDecl
        nname = convertCamelToSnake(structt['name']) + "_" + funName
        s += "def "+nname + "(" + args + "):"
        return s


    def genFieldList(self,structt,fldList,nameprefix,seperator,inclAssign,inclTypes=False,noLastSeperator=True):
        s = ""
        ss = s
        for fld in fldList:
            s += nameprefix+fld["name"] 
            if "annoMsgTag" in fld and inclAssign == True:
                # if it is a @TAG field, find the retag if present
                retagval = self.findStructRetag(structt,fld)
                s += " = " + retagval
            elif "value" in fld and inclAssign == True:
                s += " = " + fld["value"]
            ss = s    # store string without the last seperator
            s += seperator 
        if noLastSeperator:
            return ss 
        return s
    def findStructRetag(self,structt,field):
        if "retags" in structt:
            retags    = structt["retags"]
            name = field["name"]
            for tag in retags:
                if tag["name"] == name:
                    return tag["expr"]
        return "!ERR retag not defined!"
                # find the message/struct where the field is declared
                # (ss,ff) = self.findFieldName(structt,tag["name"])
                # if ss != None:
                #     ttype = self.lookupFieldType(ff)
                #     s += "    "+ ttype + " " + ff["name"] + " = " + tag["expr"] + ";\n"    
                # else:  s +=  "#error Error TAG not defined:" + ss["name"]


    # make2 lists containing all field based on condions
    def getFieldLists(self,structt,inclParent = True, incAssign = True, incHidden=True, incTagged=True):
        """ Make 2 lists, first with all fields that meet the conditions above 
            and a 2nd list with all the other fields.
            Note if inclParent == FALSE only the local fields will be in the lists

            %param incAssign  includes fields that are assigned
        """
        lst     = []
        notlist = []
        fields    = structt["body"]
        s = ""
        if inclParent and "parentName" in structt:
            parnt = self.getParent(structt)
            (lst,notlist) = self.getFieldLists(parnt,inclParent, incAssign, incHidden, incTagged)
        for fld in fields:
            if "value" in fld and incAssign == False:
                notlist.append(fld)
            elif fld["name"].startswith("__") and incHidden == False :

                notlist.append(fld)
            elif "annoMsgTag" in fld and  incTagged == False:
                notlist.append(fld)
            else:
                lst.append(fld)
        return (lst,notlist)
    def genClassDeclBegin(self,structname,parentstruct= None):
        s = "class "+structname
        if parentstruct is not None:
            s += "(" + parentstruct + ")"
        return s + ":\n"
    def genClassDeclEnd(self,structname):
        return "\n\n"
    def makeConstructArgList(self,structt):
        return self.makeFieldListGeneric(structt,False, seperator = "," , inclParent = True , inclConst = False, namePrefix = "_" )

    def genClassConstructor(self,structt):
        s  = IND1 + self.makeLineComment( "Constructor")
        #args = self.makeConstructArgList(structt)
        # split the fields into two lists, locally assigned fields and fieldvalues passed in as arguments
        # (ll,nl) = self.getFieldLists(structt,incHidden=False, incAssign = False) 
        # args = self.genFieldList(structt,ll,"",",",inclAssign=False)
        # s += indnt(1) + "def __init__(self,"+args+"):" + "\n"
        # TODO: clear above?  
        # split the fields into two lists, locally assigned fields and fieldvalues passed in as arguments
        (ll,nl) = self.getFieldLists(structt,inclParent = True, incAssign = False, incHidden=True, incTagged=False) 
        args = self.genFieldList(structt,ll,"",",",inclAssign=False)
        s += IND1 + "def __init__(self,"+args+"):" + "\n"
        # generate constant fields
        s += IND2 + self.genFieldList(structt,nl,"self.",NL+IND2,inclAssign=True) +NL
        # generate fields and assignments of data passed as argument
        for i in ll:
            s  += IND2+"self." + i["name"] + " = " + i["name"] +NL
        return s
    def genAll(self,fname):
        s  = "from enum import Enum\n"
        s += annotateDict.get('py_imports',"")
        s += annotateDict.get('py_code',"")
        s += self.genAllEnumDefs(separator = "")
        for st in structList:
            s += self.genClassDeclAndImplement( st) +NL

        return s

